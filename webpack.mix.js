let mix = require('laravel-mix');
let Webpack = require('webpack');
let path = require('path');
const nodeExternals = require('webpack-node-externals');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
let externals, target, devtool;
if (process.env.NODE_ENV === 'test') {
   externals = [nodeExternals()];
   target = 'node';
   devtool = 'inline-cheap-module-source-map';
} else {
   externals = [];
   target = 'web';
   devtool = 'eval-source-map';
}

mix.webpackConfig({
      module: {
         rules: [
            {
               test: /\.tsx?$/,
               loader: 'ts-loader',
               options: {
                  appendTsSuffixTo: [ /\.vue$/ ]
               },
               exclude: /node_modules/
            }
         ]
      },
      resolve: {
         extensions: ['*', '.js', '.jsx', '.vue', '.ts', '.tsx'],
         alias: {
            fonts: path.resolve(__dirname, 'public/fonts')
         }
      },
      plugins: [
         new Webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.$' :  'jquery',
            'window.jQuery' :  'jquery',
         })
      ],
      externals: externals,
      target: target,
      output: {
         publicPath: 'public'
      }
   })
   .babelConfig({
      plugins: [["@babel/plugin-proposal-decorators", {"decoratorsBeforeExport": true}], "@babel/plugin-transform-runtime"],
      presets: ["@babel/preset-env" , "@babel/preset-typescript"]
   })
   .js('resources/assets/js/app.js', 'public/js')
   .js('resources/assets/js/admin.js', 'public/js')
   .scripts([
      'node_modules/html5shiv/dist/html5shiv.min.js', 
      'node_modules/respond.js/dest/respond.min.js', 
      'node_modules/selectivizr/selectivizr.js'
  ], 'public/js/compatibility.js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   //.sass('resources/assets/sass/font-awesome-4.7.0/scss/font-awesome.scss', 'public/css')
   .sourceMaps(true, devtool);
   //.version();
