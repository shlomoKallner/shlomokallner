<?php

namespace App;

//use Illuminate\Database\Eloquent\Relations\Pivot;
use App\PivotBase, 
    App\Image, 
    App\User,
    App\Utilities\Functions;


class UserImages extends PivotBase
{
    
    /// these 3 methods require overides in the implementing class.

    static public function getOthersKey() {
        return 'image_id';
    }

    static public function getIdFromOther($other) {
        $img = Image::getFrom($other);
        return Functions::testVar($img) ? $img->id : null;
    }

    static public function getOtherClassName() {
        return 'App\Image';
    }

    abstract static public function getThisDataKey() {
        return 'user_id';
    }

    abstract static public function getThisDataClassName() {
        return 'App\User';
    }

    static public function getIdFromThisData($thisData) {
        $user = User::getFrom($thisData);
        return Functions::testVar($user) ? $user->id : null;
    }
}
