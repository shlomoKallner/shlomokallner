<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialInfo extends Base
{
    public function user()
    {
        return $this->belongsTo(
            'App\User', 'user_id', 'id'
        );
    }
}
