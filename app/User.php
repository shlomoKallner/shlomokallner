<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use App\Utilities\Functions;
use Illuminate\Support\Facades\Hash;

class User extends Base implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, Notifiable;
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function createNew(string $name, string $email, string $password, $img, bool $remember_token = false) {
        $tmp = self::where(
            [
                ['name', '=', $name],
                ['email', '=', $email]
            ]
        )->get();
        if (!Functions::testVar($tmp) || !Functions::countHas($tmp)) {
            $data = new self;
            $data->name = $name;
            $data->email = $email;
            $data->password = Hash::make($password);
            $data->setRememberToken($remember_token ? str_random(100) : '');
            if ($data->save()) {
                return $data;
            }
        }
        return null;
    }

    static public function createNewFrom(array $array, bool $retObj = false) {
        $name = Functions::getPropKey($array, 'name', null);
        $email = Functions::getPropKey($array, 'email', null);
        $password = Functions::getPropKey($array, 'password', null);
        $img = Functions::getPropKey($array, 'img', null);
        $remember_token = Functions::getPropKey($array, 'remember_token', false);
        if (Functions::testVar($name) && is_string($name)
            && Functions::testVar($email) && is_string($email)
            && Functions::testVar($password) && is_string($password)
            && Functions::testVar($remember_token) && is_bool($remember_token)
        ) {
            $data = self::createNew($name, $email, $password, $img, $remember_token);
            if (Functions::testVar($data)) {
                return $retObj ? $data : $data->id;
            } 
        }
        return null;
    }

    public function updateThis(string $name = '', string $email = '', string $password = '', $img = null, $remember_token = null) {
        $newName = Functions::testVar($name) && $name !== $this->name ? $name : $this->name;
        $newEmail = Functions::testVar($email) && $email !== $this->email ? $email : $this->email;
        
        $tmp = self::where(
            [
                ['name', '=', $newName ],
                ['email', '=', $newEmail],
                ['id', '<>', $this->id]
            ]
        )->get();
        if (!Functions::testVar($tmp) || !Functions::countHas($tmp)) {
            if (Functions::testVar($name) && $name !== $this->name) {
                $this->name = $name;
            }
            if (Functions::testVar($email) && $email !== $this->email) {
                $this->email = $email;
            }
            if (Functions::testVar($password) && !Hash::check($password, $this->password)) {
                $this->password = Hash::make($password);
            }
            if (is_bool($remember_token)) {
                if ($remember_token === true) {
                    $this->setRememberToken(str_random(100));
                } else {
                    $this->setRememberToken('');
                }
            }
            return $this->save();
        }
        return null;
    }

    public function roles() {
        return $this->hasMany('App\UserRole', 'id', 'user_id');
    }
}
