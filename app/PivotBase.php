<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;
use App\Utilities\Functions,
    App\Utilities\Traits\Model as ModelTrait;

abstract class PivotBase extends Pivot
{
    use ModelTrait;

    static public function createNew($thisData, $other, bool $retObj = false)
    {
        $otherClassName = self::getOtherClassName();
        $thisDataClassName = self::getThisDataClassName();
        if ($thisData instanceof $thisDataClassName && $other instanceof $otherClassName) {
            $otherKey = self::getOthersKey();
            $otherId = self::getIdFromOther($other);
            $thisDataKey = self::getThisDataKey();
            $thisDataId = self::getIdFromThisData($thisData);
            $tmp = self::where(
                [
                    [$otherKey, '=', $otherId],
                    [$thisDataKey, '=', $thisDataId],
                ]
            )->get();
            if (!Functions::testVar($tmp) || !Functions::countHas($tmp)) {
                $data = new self;
                $data->{ $otherKey } = $otherId;
                $data->{ $thisDataKey } = $thisDataId;
                if ($data->save()) {
                    return $retObj 
                        ? $data
                        : $data->id;
                }
            } elseif (count($tmp) === 1) {
                return $retObj ? $tmp[0] : $tmp[0]->id;
            }
        }
        return null;
    }

    // this one is not final-ed because some Use-rs may have need to 
    //  overide it with further ways of retrieval..
    static public function getFrom($item, bool $withTrashed = true)
    {
        if (Functions::testVar($item)) {
            $otherClassName = self::getOtherClassName();
            $thisDataClassName = self::getThisDataClassName();
            if ($item instanceof self) {
                return $item;
            } elseif (is_int($item)) {
                return self::getFromId($item, $withTrashed);
            } else {
                $id = '';
                $key = '';
                if ($item instanceof $otherClassName) {
                    $id = self::getIdFromOther($item);
                    $key = self::getOthersKey();
                } elseif ($item instanceof $thisDataClassName) {
                    $id = self::getIdFromThisData($item);
                    $key = self::getThisDataKey();
                }
                if (Functions::testVar($id) && Functions::testVar($key)) {
                    return $withTrashed
                    ? self::withTrashed()->where($key, $id)->first()
                    : self::where($key, $id)->first();
                }
            }
        } 
        return null;
    }

    static public function getIdFrom($item, bool $usePublic = true, $def = 0) 
    {
        $item_id = $def;
        $otherClassName = self::getOtherClassName();
        $thisDataClassName = self::getThisDataClassName();
        if ($item instanceof self) {
            $item_id = $usePublic 
            ? $item->getPubId()
            : $item->id;
        } elseif (is_int($item) && self::existsId($item, true)) {
            $item_id = $item;
        } elseif ($item instanceof $otherClassName) {
            $item_id = self::getIdFrom(self::getFrom($item, true), $usePublic, $def);
        } elseif ($item instanceof $thisDataClassName) {
            $item_id = self::getIdFrom(self::getFrom($item, true), $usePublic, $def);
        } elseif (Functions::isPropKeyIn($item, 'id')) {
            $item_id = Functions::getPropKey($item, 'id', $def);
        } 
        return $item_id;
    }

    public function other()
    {
        return $this->belongsTo(
            self::getOtherClassName(), self::getOthersKey()
        );
    }

    public function thisData()
    {
        return $this->belongsTo(
            self::getThisDataClassName(), self::getThisDataKey()
        );
    }

    /// these 3 methods require overides in the implementing class.

    abstract static public function getOthersKey();

    abstract static public function getIdFromOther($other);

    abstract static public function getOtherClassName();

    abstract static public function getThisDataKey();

    abstract static public function getThisDataClassName();

    abstract static public function getIdFromThisData($thisData);
}
