<?php

namespace App\Utilities\Traits;

use Illuminate\Http\Request,
    Illuminate\Http\Response,
    Illuminate\Http\JsonResponse,
    Symfony\Component\VarDumper\Cloner\VarCloner,
    Symfony\Component\VarDumper\Dumper\HtmlDumper,
    Symfony\Component\VarDumper\Dumper\CliDumper,
    Illuminate\Support\Facades\Log,
    App\Exceptions\JsonException;

trait Exception
{
    
 
    static public function jsonRetOrDump(
        Request $request, $id = null, ...$data
    ) {
        if (! $request->ajax()) {
            dd($request, $id ?? __METHOD__, ...$data);
        } else {
            throw new JsonException($request, $id ?? __METHOD__, ...$data);
        }
    }

    static public function genDumpResponse(Request $request, ...$data)
    {
        if ($request->ajax()) {
            $tmp = [];
            $dumper = new CliDumper;
            $tmp[] = $dumper->dump((new VarCloner)->cloneVar($request), true);
            foreach ($data as $val) {
                $tmp[] = $dumper->dump((new VarCloner)->cloneVar($val), true);
            }
            Log::info('ajax message in '. __METHOD__ , $tmp);
            return new JsonResponse($tmp);
        } else {
            $dumper = new HtmlDumper;
            $data[] = $request;
            $tmp = $dumper->dump((new VarCloner)->cloneVar($data), true);
            Log::info('html message in '. __METHOD__ , $tmp);
            return new Response($tmp);
        }
    }
}