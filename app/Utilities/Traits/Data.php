<?php

namespace App\Utilities\Traits;

use Illuminate\Support\Collection,
    Countable, ArrayAccess, Traversable,
    // IteratorAggregate, 
    Illuminate\Contracts\Support\Htmlable,
    Illuminate\Contracts\Support\Arrayable, 
    Composer\Semver\Comparator;
    

trait Data
{
    
    static public function testVar($var)
    {
        return isset($var) && !empty($var);
    }

    static public function testVarOrId($var)
    {
        if (self::testVar($var)) {
            if (is_int($var) || is_float($var)) {
                return $var > 0;
            } else {
                return true;
            }
        }
        return false;
    }

    static public function getVar($var, $default = null)
    {
        return self::testVar($var) ? $var : $default;
    }

    static public function getVarOrId($var, $default = null)
    {
        return self::testVarOrId($var) ? $var : $default;
    }

    static public function testBladedVar($var)
    {
        if (self::testVar($var)) {
            if (is_string($var)) {
                $tmp = unserialize(html_entity_decode((string)$var));
                return !empty($tmp);
            } elseif ($var instanceof Htmlable) {
                $tmp = unserialize(html_entity_decode($var->toHtml()));
                return !empty($tmp);
            }
            return true;
        }
        return false;
    }

    /**
     * Function getBladedContent -  Get the content of a 
     *                              Blade Escaped Serialized
     *                              variable..
     *
     * @param mixed|string $var
     * @param mixed $default
     * @return mixed
     */
    static public function getBladedContent($var, $default = null)
    {
        if (self::testVar($var)) {
            if (is_string($var) ) {
                $tmp = unserialize(html_entity_decode((string)$var));
                return !empty($tmp) ? $tmp : $default ;
            } elseif ($var instanceof Htmlable) {
                $tmp = unserialize(html_entity_decode($var->toHtml()));
                return !empty($tmp) ? $tmp : $default ;
            } else {
                return $var;
            }
        } 
        return $default;
    }

    /** 
     * Function getUnBladedContent - Get the content of a 
     *                               not Blade Escaped  but 
     *                               Serialized variable..
     *
     * @param mixed|string $var
     * @param mixed $default
     * @return mixed
    */
    static public function getUnBladedContent($var, $default = null)
    {
        if (self::testVar($var)) {
            if (is_string($var) ) {
                $tmp = unserialize((string)$var);
                return !empty($tmp) ? $tmp : $default ;
            } elseif ($var instanceof Htmlable) {
                $tmp = unserialize($var->toHtml());
                return !empty($tmp) ? $tmp : $default ;
            } else {
                return $var;
            }
        } 
        return $default;
    }

    /** 
     * Function getBladedString - Unescape a Blade Escaped string.
     *
     * @param mixed|string|Htmlable $str
     * @param mixed $default
     * @return mixed
    */
    static public function getBladedString($str, $default = '')
    {
        if (self::testVar($str)) {
            $tmp = null;
            if (is_string($str)) {
                $tmp = html_entity_decode((string)$str);
            } elseif ($str instanceof Htmlable) {
                $tmp = html_entity_decode($str->toHtml());
            }
            return !empty($tmp) ? $tmp : $default ;
        }
        return $default;
    }

    static public function toBladableContent($val)
    {
        if ($val instanceof Htmlable) {
            return $val->toHtml();
        } elseif (is_array($val) || is_object($val)) { 
            return serialize($val);
        } else { 
            return $val;
        }
    }

    static public function is_countable($value)
    {
        if (isset($value)) {
            return is_array($value) 
            || ($value instanceof Countable && $value instanceof Traversable);
        }
        return false;
    }

    static public function countHas($value)
    {
        return self::is_countable($value) && count($value) > 0;
    }

    static public function arrayableToArray($arr, $def = null)
    {
        if (self::testVar($arr)) {
            if ($arr instanceof Collection) {
                return $arr->all();
            } elseif ($arr instanceof Arrayable) {
                return $arr->toArray();
            } elseif (is_array($arr)) {
                return $arr;
            }
        }
        return $def;
    }

    static public function isPropKeyIn($data, $name, $def = null) 
    {
        $bol = $def;
        if (isset($data) && self::testVar($name)) {
            if (is_array($data) && is_int($name)) {
                $bol = array_key_exists($name, $data);
            } elseif ((is_array($data) || $data instanceof ArrayAccess) && is_string($name)) {
                $bol = array_has($data, $name); 
            } elseif (is_object($data)) {
                $bol = property_exists($data, $name) || isset($data->$name) 
                    || !empty($data->$name); 
            }
        }
        return $bol;
    }

    static public function getPropKey($data, $name, $default = null) 
    {
        if (isset($data) && self::testVar($name)) {
            if (self::isPropKeyIn($data, $name)) {
                //$res = $default; // null;
                if (is_array($data) || $data instanceof ArrayAccess) {
                    return $data[$name];
                } elseif (is_object($data)) {
                    return $data->$name;
                }
            }
            //return $res;
        } 
        return $default;
    }

    static public function hasPropKeyIn($data, $name, $def = null)
    {
        $bol = $def;
        if (isset($data) && self::testVar($name)) {
            $bol = self::testVar(self::getPropKey($data, $name, $def));
        }
        return $bol;
    }

    static public function setPropKey(&$data, $name, $val = null)
    {
        //$res = self::isPropKeyIn($data, $name);
        if (isset($data) && self::testVar($name)) {
            if (is_array($data) || $data instanceof ArrayAccess) {
                $data[$name] = $val;
                return true;
            } elseif (is_object($data)) {
                $data->$name = $val;
                return true;
            }
        } 
        //dd($res, $data, $name, $val, is_array($data));
        //return $res;
        return false;
    }

    static public function testVersions(
        string $ver1, string $ver2, string $op = '=='
    ) {
        $res = false;
        switch ($op) {
            case '==':
                $res = Comparator::equalTo($ver1, $ver2);
                break;

            case '!=':
                $res = Comparator::notEqualTo($ver1, $ver2);
                break;
            
            case '>':
                $res = Comparator::greaterThan($ver1, $ver2);
                break;
            
            case '<':
                $res = Comparator::lessThan($ver1, $ver2);
                break;

            case '>=':
                $res = Comparator::greaterThanOrEqualTo($ver1, $ver2);
                break;

            case '<=':
                $res = Comparator::lessThanOrEqualTo($ver1, $ver2);
                break;

            default:
                $res = false;
                break;
        }
        return $res;
    }
}