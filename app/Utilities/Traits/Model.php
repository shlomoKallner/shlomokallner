<?php

namespace App\Utilities\Traits;

use Illuminate\Database\Eloquent\SoftDeletes;

trait Model
{
    use SoftDeletes;

    // public const DELETED_AT = 'deleted_at';

    /**
     * Get the name of the "deleted at" column.
     *
     * @return string
     */
    public function getDeletedAtColumn()
    {
        return 'deleted_at';
    }


    final static public function getFromId(int $id, bool $withTrashed = false) 
    {
        return $withTrashed 
        ? self::withTrashed()->where('id', $id)->first()
        : self::where('id', $id)->first();
    }

    final static public function existsId(int $id, bool $withTrashed = false) 
    {
        return Functions::testVar(self::getFromId($id, $withTrashed));
    }

    // final-ed because it just uses getFrom()..
    final static public function exists($item, bool $withTrashed = true)
    {
        return Functions::testVar(self::getFrom($item, $withTrashed));
    }

    // this one is not final-ed because some Use-rs may have need to 
    //  overide it with further ways of retrieval..
    static public function getFrom($item, bool $withTrashed = true)
    {
        if (Functions::testVar($item)) {
            if ($item instanceof self) {
                return $item;
            } elseif (is_int($item)) {
                return self::getFromId($item, $withTrashed);
            } 
        } 
        return null;
    }

    final public function getDatesArray()
    {
        $dates = Functions::genDatesArray(
            $this->created_at, $this->updated_at,
            $this->deleted_at
        );
        return $dates;
    }

    public function getPubId()
    {
        return $this->id;
    }
    
}