<?php

namespace App\Utilities\Traits;

use Illuminate\Support\Carbon;

trait Date
{
    static public function getDateTimeStr(
        string $sep = '', string $dateSep = '/',
        string $timeSep = ':'
    ) {
        $date = 'd' . $dateSep . 'm' . $dateSep . 'Y';
        $time = 'h' . $timeSep . 'i' . $timeSep . 's';
        return date('D' . $sep . $date . $sep . $time);
    }

    static public function genDatesArray(
        $created_at, $updated_at, $deleted_at
    ) {
        return [
            'created' => $created_at,
            'modified' => $updated_at,
            'deleted' => $deleted_at
        ];
    }

    static public function genDefaultDates(bool $setUpdated = false) 
    {
        $now = Carbon::now();
        return self::genDatesArray(
            $now, $setUpdated ? $now : null, null
        );
    }

}
