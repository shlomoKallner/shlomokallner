<?php

namespace App\Utilities\Traits;

use Illuminate\Support\Facades\Log;

trait Paging
{
    

    static public function genRange(int $beg, int $end, int $step = 1)
    {
        $tmp = [];
        if ($step > 0) {
            for ($i = $beg; $i <= $end; $i += $step) {
                $tmp[] = $i;
            }
        } elseif ($step < 0) {
            for ($i = $beg; $i >= $end; $i += $step) {
                $tmp[] = $i;
            }
        } else {
            $tmp[] = $beg;
            $tmp[] = $end;
        }
        return $tmp;
    }

    /**
     * Function genRowsPerPage
     * 
     * Should have been called getNumRowsPerPage ..
     * or getNumItemsPerUnit ..
     * TOO LATE!
     * 
     * Can also be used for determining the number 
     * of Pages of content, using the total
     * number of content items as $ppp (productsPerPage)
     * and the number of Items per Page as $ppr (productsPerRow).
     * 
     * @param integer $ppp - productsPerPage
     * @param integer $ppr - productsPerRow
     *
     * @return integer      - numRowsPerPage
     */
    static public function genRowsPerPage(int $ppp, int $ppr) 
    {
        if ($ppp <= $ppr) {
            return 1;
        } else {
            $res = intdiv($ppp, $ppr);
            if ($ppp % $ppr > 0) {
                $res++;
            }
            return $res;
        }
    }

    static public function getPageFromArray(array $items, int $page, int $perPage)
    {
        //$offset = max(0, ($page - 1) * $perPage);
        $offset = max(0, $page * $perPage);
        return array_slice($items, $offset, $perPage, true);
    }


    /**
     * Function genPageArray
     *
     * @param  array   $range      - must be a Range of Indexes from 
     *                             genRange() or similar array!
     * @param  integer $numPerPage
     * 
     * @return array
     * 
     */
    static public function genPageArray(array $range, int $numPerPage)
    {
        $res = [];
        $rngLen = count($range);
        if ($rngLen <= $numPerPage) {
            $res[] = $range;
        } elseif ($rngLen > $numPerPage) {
            // $col = collect($range);
            $numTotal = self::genRowsPerPage($rngLen, $numPerPage);
            for ($i = 0; $i < $numTotal; $i++ ) {
                // Collection::forPage() cannot really handle a page
                //  numbered '0' -- if '0' is passed in it will 
                //  result in a duplicate page on passing in 
                //  page number '1'..
                // $tmp = $col->forPage($i+1, $numPerPage)->all();
                $tmp = self::getPageFromArray($range, $i, $numPerPage);
                $tmp1 = [];
                foreach ($tmp as $val) {
                    $tmp1[] = $val;
                }
                $res[] = $tmp1;
            }
        }
        return $res;
    }

    static public function getLastIndex(array $arr)
    {
        return Data::testVar($arr) ? count($arr) - 1 : 0;
    }

    static public function getIndexOf(array $arr, $val)
    {
        $res = null;
        if (Data::testVar($arr) && Data::testVar($val)) {
            foreach ($arr as $key => $value) {
                if ($value === $val) {
                    $res = $key;
                    break;
                }
            }
        } 
        return $res;
    }

    /**
     *  Function genPagesIndexes
     * 
     * @param integer $ppp      - productsPerPage
     * @param integer $ppr      - productsPerRow
     * @param int     $tp       - totalProducts (total number OF products)
     * @param int     $pn       - pageNumber ; valid page numbers are from 
     *                          0 and up;
     *                          - -1 for generate and return all as a 
     *                          single-page;
     *                          - -2 for generate and return all pages;
     * @param int     $numPages - the number of pages of products as 
     *                          calculated by this function and returned 
     *                          to the caller by reference. 
     *                          - This parameter is passed by reference.
     * 
     * @return array - a array of arrays of indices of rows..
     */
    static public function genPagesIndexes(
        int $ppp, int $ppr, int $tp, int $pn = -1,
        int &$numPages = 0
    ) {
        $res = [];
        Log::info('$numPages before: ', ['$numPages' => $numPages]);
        if ($numPages < 1) {
            $numPages = self::genRowsPerPage($tp, $ppp);
        }
        //dd($ppp, $ppr, $tp, $pn, $numPages);
        /* Log::info(
            'all parameter in '. __METHOD__ .': ', 
            [
                'itemsPerPage' => $ppp, 
                'itemsPerRow' => $ppr, 
                'totalItems' => $tp, 
                'pageNumber' => $pn,
                'numPages' => $numPages
            ]
        ); */
        if ($pn > -3 && $pn < $numPages) {

            /// step 0] if not given a valid page index, 
            ///         goto step 4.
            /// step 1] generate a 'table/array' of 
            ///         indexes into the ProductArray Per Content Page. 
            $pnValid = $pn > -1 && $pn < $numPages;
            /// step 2] split each indexesPerContentPage Array into
            ///         rows of indexesPerContentPage per Page
            /// step 3] return the page's rowArray. exit function.
            /// step 4] execute step 1, 2 and 3 with the total number of
            ///         Products as the number of Products Per Content
            ///         Page and a Page Number of 0. 
            ///         (aka Generate a PageTable with only 1 entry,
            ///          and return the entry's rowArray.)
            /* Log::info(
                'all parameter and vars in '. __METHOD__ .': ', 
                [
                    'itemsPerPage' => $ppp, 
                    'itemsPerRow' => $ppr, 
                    'totalItems' => $tp, 
                    'pageNumber' => $pn,
                    'numPages' => $numPages,
                    'isPageNumberValid' => $pnValid
                ]
            ); */
            if ($pnValid || $pn == -2) {
                // generate all pages_of_indexes ...
                // for multiple/single_selected display_page..
                $pageIndexRanges = self::genPageArray(
                    self::genRange(0, $tp - 1), $ppp
                );
                /* Log::info(
                    'all parameter and vars in '. __METHOD__ .': ', 
                    [
                        'itemsPerPage' => $ppp, 
                        'itemsPerRow' => $ppr, 
                        'totalItems' => $tp, 
                        'pageNumber' => $pn,
                        'numPages' => $numPages,
                        'isPageNumberValid' => $pnValid,
                        'pageIndexRanges' => $pageIndexRanges,
                    ]
                ); */
            } else {
                // generate a single page_of_indexes with all products..
                // for a single display_page of all products..
                $pageIndexRanges = self::genPageArray(
                    self::genRange(0, $tp - 1), $tp
                );
            }
            // now create the Pages_Of_Rows ...
            if ($pnValid || $pn == -1) {
                // if generating for a single_selected_display_page
                // or for a single_display_page_of_all_products ..
                $tmpArray = $pageIndexRanges[$pnValid?$pn:0];
                $tmpRange = self::genRange(
                    $tmpArray[0], 
                    $tmpArray[self::getLastIndex($tmpArray)]
                );
                $res[] = self::genPageArray($tmpRange, $ppr);
                /* Log::info(
                    'all parameter and vars in '. __METHOD__ .': ', 
                    [
                        'itemsPerPage' => $ppp, 
                        'itemsPerRow' => $ppr, 
                        'totalItems' => $tp, 
                        'pageNumber' => $pn,
                        'numPages' => $numPages,
                        'isPageNumberValid' => $pnValid,
                        'pageIndexRanges' => $pageIndexRanges,
                        'tmpIsOf1' => [
                            $pageIndexRanges[$pnValid?$pn:0][0], 
                            count($pageIndexRanges[$pnValid?$pn:0]) -1
                        ],
                        'tmpIsOf12' => [
                            $tmpArray[0], 
                            $tmpArray[self::getLastIndex($tmpArray)]
                        ],
                        'tmpRange' => $tmpRange,
                        'res' => $res,
                    ]
                ); */
                //dd("res", $res, $tmpRange);
            } else {
                // if generating all display_pages ..
                foreach ($pageIndexRanges as $page) {
                    $res[] = self::genPageArray(self::genRange(0, count($page)), $ppr);
                }
            }
        }
        return $res;
    }

}