<?php

namespace App\Utilities\Traits;

use Illuminate\Contracts\Support\Htmlable, 
    HTMLPurifier;

trait Purifier
{
    static public function purifyContent($content)
    {
        if (Data::testVar($content)) {
            $purify = new HTMLPurifier();
            if (is_string($content)) {
                return $purify->purify($content);
            } elseif ($content instanceof Htmlable) {
                return $purify->purify($content->toHtml());
            } elseif (is_object($content)) {
                foreach ($content as $key => $value) {
                    $content->$key = self::purifyContent($value);
                }
                return $content;
            } elseif (is_array($content)) {
                foreach ($content as $key => $value) {
                    $content[$key] = self::purifyContent($value);
                }
                return $content;
            } else {
                return $purify->purify((string)$content);
            }
        } 
        return null;
    }
}