<?php

namespace App\Utilities;

use Illuminate\Http\Request;

use App\Utilities\Traits\Data,
    App\Utilities\Traits\Exception,
    App\Utilities\Traits\Date,
    App\Utilities\Traits\Purifier,
    App\Utilities\Traits\Paging;

class Functions
{
    use Data, Exception, Date, Purifier, Paging;

    static public function getImageFileMIMETypeStr()
    {
        return 'image/jpeg,image/gif,image/png';
    }

    static public function isAdminPath($path)
    {
        if (is_string($path)) {
            return in_array('admin', explode('/', $path));
        } elseif ($path instanceof Request) {
            return self::isAdminPath($path->path());
        } 
        return false;
    }

    static public function genMultipleFromArray(array $arr, int $num)
    {
        $res = [];
        if (self::testVar($arr) && self::testVar($num) && $num > 0) {
            for ($i = 0; $i < $num; $i++) {
                foreach ($arr as $val) {
                    $res[] = $val;
                }
            }
        }
        return $res;
    }

    static public function getLoremIpsum()
    {
        return str_replace(
            ["\r\n", "\r", "\n"], ' ', e(
                'Lorem ipsum dolor sit amet, 
        consectetur adipisicing elit. Modi, nulla, 
        porro facilis officiis sequi natus eum nemo 
        totam eius deserunt reprehenderit ducimus quia et 
        itaque animi nostrum adipisci accusantium. 
        Quaerat, eos ipsum expedita totam dolorem rem 
        reiciendis voluptatibus quia dolor quam natus 
        id ipsam aliquam fugiat ullam quibusdam unde 
        corporis minima debitis odit laborum numquam 
        repellat illo ea aut mollitia alias? Ut, facere, 
        inventore, mollitia consectetur cum repellat quidem 
        qui itaque modi quam laudantium cupiditate a nemo officia 
        deserunt laboriosam temporibus unde voluptate suscipit labore 
        voluptates cumque quas natus non in maiores dicta delectus omnis 
        aut commodi animi molestiae amet fugit? Tenetur, eligendi, 
        a pariatur laboriosam aliquid cum voluptate nisi 
        laudantium officiis in voluptatum nihil libero consequatur 
        tempora sunt dolorum beatae dicta quod illo impedit!'
            )
        );
    }

}