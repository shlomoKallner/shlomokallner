<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Utilities\Functions,
    App\Utilities\Traits\Model as ModelTrait;

abstract class Base extends Model
{
    use ModelTrait;

    abstract static public function createNewFrom(array $array, bool $retObj = false);
}
