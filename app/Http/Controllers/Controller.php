<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Utilities\Functions,
    Illuminate\Support\HtmlString;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    static public $data = [
        'site' => [
            'lang' => 'en',
            'title' => '',
            'title_sep' => ' | ',
            'logo' => [
                'main' => [],
                'stick' => [],
            ]
        ],
        'page' => [
            'entries' => [
                [
                    'title' => 'BEST FOOD',
                    'subTitle' => 'create your own slogan',
                    'extraCss' => '',
                    'img' => [
                        'img' => '',
                    ],
                    'pos' => '',
                ]
            ],
            'about' => [
                [
                    'text' => 'Astronomy compels the soul to look upward, and leads us from this world to another.  Curious that we spend more time congratulating people who have succeeded than encouraging people who have not. As we got further and further away, it [the Earth] diminished in size.',
                    'extraCss' => '',
                ]
            ],
            'gallery' => [
                'items' => '',
                'sortings' => '',
                'title' => '',
            ],
            //'' => '',
        ],
        'alerts' => [],
    ];



    public function __construct()
    {
        //parent::__construct();
        self::setSiteData(
            'logo.main', 
            [
                'img' => asset('lib/assets/images/Logo_main.png'),
                'alt' => 'the Site Logo',
            ]
        );
        self::setSiteData(
            'logo.stick', 
            [
                'img' => asset('lib/assets/images/Logo_stick.png'),
                'alt' => 'the Site Logo',
            ]
        );
    }

    /**
     * A helper function for setting members of the 'site'
     * member of the static data variable...
     * without necessarily overwriting presets...
     * 
     */
    static protected function setData(string $dKey, $content, $val = null, callable $filter = null) 
    {
        if (!empty($content)) {
            if (is_string($content)) {
                if (!empty($val)) {
                    $tmp = !is_null($filter) && is_callable($filter) ? $filter($val) : $val;
                    array_set(self::$data[$dKey], $content, $tmp);
                }
            } elseif ($content instanceof HtmlString && !empty($val)) {
                self::setData($dKey, $content->toHtml(), $val, $filter);
            } elseif (is_array($content) || is_object($content)) {
                foreach ($content as $key => $value) {
                    self::setData($dKey, $key, $value, $filter);
                }
            } 
        }
    }

    static protected function getData(string $dKey, string $key, $default = null)
    {
        if (Functions::testVar($key)) {
            $tmp = array_get(self::$data[$dKey], $key, $default);
            return Functions::getVar($tmp, $default);
        } else {
            return $default;
        }
    }

    static protected function hasData(string $dKey, $key)
    {
        if (Functions::testVar($key)) {
            return array_has(self::$data[$dKey], $key);
        } else {
            return null;
        }
    }

    /**
     * A helper function for setting members of the 'site'
     * member of the static data variable...
     * without necessarily overwriting presets...
     * 
     */
    static public function setSiteData($content, $val = null) 
    {
        self::setData('site', $content, $val);
    }

    static public function getSiteData(string $key, $default = null)
    {
        return self::getData('site', $key, $default);
    }

    static public function hasSiteData($key)
    {
        return self::hasData('site', $key);
    }

    /**
     * A helper function for setting members of the 'page'
     * member of the static data variable...
     * without necessarily overwriting presets...
     * 
     */
    static public function setPageData($content, $val = null) 
    {
        self::setData(
            'page', $content, $val, 
            function ($v) { 
                return Functions::purifyContent($v);
            }
        );
    }

    static public function getPageData(string $key, $default = null)
    {
        return self::getData('page', $key, $default);
    }

    static public function hasPageData($key)
    {
        return self::hasData('page', $key);
    }

    static public function setAlert(
        $css = '', $title = '', $text = '', int $timeout = 0
    ) {
        /**
         *   text: String;
         *   title: String;
         *   extraCss: String;
         *   timeout: Number;
         */
        self::$data['alerts'][] = [
            'extraCss' => Functions::purifyContent($css),
            'title' => Functions::purifyContent($title),
            'text' => Functions::purifyContent($text),
            'timeout' => $timeout,
        ];
    }

    static public function setSiteNut(Request $request, int $length = 120)
    {
        $nut = str_random($length);
        self::setSiteData('nut', $nut);
        $request->session()->put('_nut', $nut);
        return $nut;
    }

    static public function getRequestData(Request $request)
    {
        $tmp = [
            'request' => $request->all(),
            'cookies' => $request->cookies->all(),
        ];
        if ($request->hasSession()) {
            $tmp['session'] = $request->session()->all();
            $tmp['SID'] = [
                'id' => $request->session()->getId(),
                'name' => $request->session()->getName(),
            ];
        }
        return $tmp;
    }

    static public function addMsg($text, $css = '', $title = '', int $timeout = 0)
    {
        $msg = [
            'text' => Functions::purifyContent($text),
            'extraCss' => Functions::purifyContent($css),
            'title' => Functions::purifyContent($title),
            'timeout' => $timeout,
        ];
        if (session()->has('msgs')) {
            //dd(session()->all(), $msg);
            session()->push('msgs', $msg);
        } else {
            $tmp = [];
            $tmp[] = $msg;
            //dd(session()->all(), $msg);
            session()->put('msgs', $tmp);
            //dd(session(), $msg);
            
        }
        //dd(session()->all());
    }

    static public function getMsgs()
    {
        if (session()->has('msgs')) {
            $msgs = session()->pull('msgs');
            session()->forget('msgs');
            //dd(session(), $msgs);
            if (Functions::testVar($msgs)) {
                return $msgs;
            }
        } else {
            return null;
        }
    }

    static public function getView(
        Request $request, string $viewName = 'template', 
        string $title = '', $content = [], 
        array $alert = null
        //, bool $useFakeData = false, 
        // array $breadcrumbs = null, 
        // array $sidebar = null
    ) {
        self::setSiteData('title', Functions::purifyContent($title));
        self::setPageData($content);
        if (Functions::testData($alert) && Functions::countHas($alert)) {
            self::setAlert(
                $alert['extraCss'], $alert['title'], 
                $alert['text'], $alert['timeout']
            );
        }
        $msgs = self::getMsgs();
        if (Functions::testData($msgs) && Functions::countHas($msgs)) {
            foreach ($msgs as $msg) {
                self::setAlert(
                    $msg['extraCss'], $msg['title'], 
                    $msg['text'], $msg['timeout']
                );
            }
        }
        self::setSiteNut($request, 120);
        $request->session()->regenerate();
        return view($viewName, self::$data);
    }


}
