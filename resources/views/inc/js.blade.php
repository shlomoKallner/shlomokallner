


@section('js-preloaded')
    @parent
    
@endsection

@section('js-main')
    @parent

    @guest
        <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    @else
        <script type="text/javascript" src="{{ asset('js/admin.js') }}"></script>
    @endguest
    
@endsection

@section('js-defered')
    @parent
    
    {{-- <script type="text/javascript" src="{{ asset('lib/assets/js/jquery-1.11.2.min.js')}}"></script> --}}
    
    <script type="text/javascript" src="{{ asset('lib/assets/js/jquery.flexslider.min.js')}}"></script>
    <script type="text/javascript">
        $(window).on('load', function() {
            $('.flexslider').flexslider({
            animation: "slide",
            controlsContainer: ".flexslider-container"
            });
        });
    </script>

    {{-- <script type="text/javascript" src="{{ asset('lib/assets/js/bootstrap.min.js')}}"></script> --}}
    
    
    <script type="text/javascript" src="{{ asset('lib/assets/js/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('lib/assets/js/jquery.mixitup.min.js')}}" ></script>
    @if (false)
        <script type="text/javascript" src="{{ asset('lib/assets/js/wow.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('lib/assets/js/jquery.validate.js')}}"></script>
    @endif
    <script type="text/javascript" src="{{ asset('lib/assets/js/jquery.hoverdir.js')}}"></script>
    <script type="text/javascript" src="{{ asset('lib/assets/js/jQuery.scrollSpeed.js')}}"></script>
    @if (false)
        <script type="text/javascript" src="{{ asset('lib/assets/js/reserve.js')}}"></script>
        <script type="text/javascript" src="{{ asset('lib/assets/js/contact.js')}}"></script>
    @endif
    <script type="text/javascript" src="{{ asset('lib/assets/js/script.js')}}"></script>
@endsection