



@section('css-preloaded')
    @parent 

    @if (false)

        <link rel="stylesheet" href="{{asset('lib/assets/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('lib/assets/css/font-awesome.min.css')}}">
    
    @endif

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    @if (true)
        <link rel="stylesheet" href="{{ asset('lib/font-awesome-4.7.0/css/font-awesome.min.css') }}">
    @endif

    <link rel="stylesheet" href="{{asset('lib/assets/css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('lib/assets/css/owl.theme.css')}}">
    <link rel="stylesheet" href="{{asset('lib/assets/css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('lib/assets/css/flexslider.css')}}">
    @if (false)
        <!-- the fallowing file just doesnt exist in the distro.. so removibg for now.. -->
        <link rel="stylesheet" href="{{asset('lib/assets/css/jquery.fullPage.css')}}">
    @endif
    <link rel="stylesheet" href="{{asset('lib/assets/css/pricing.css')}}">
    <link rel="stylesheet" href="{{asset('lib/assets/css/main.css')}}">

@endsection