
@php
    use \App\Utilities\Functions;
    $lang2 = Functions::getBladedString($site['lang']??'', app()->getLocale()); // 'en';
    $title2 = Functions::getBladedString($site['title']??'', config('app.name', 'Laravel'));
    $usingCDNs2 = Functions::getBladedString($site['usingCDNs']??'', false);
@endphp

<!DOCTYPE html>
<html lang="{{ $lang2 }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">

        {{-- <link rel="shortcut icon" href="{{ asset('lib/assets/images/star.png')}}" type="favicon/ico" /> --}}
        
        @section('header-metas')
            
        @show
        
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ $title2 }}</title>

        <!-- CSS START -->
            <!-- Fonts START -->
                @section('css-fonts')
                    
                @show

                @section('css-extra-fonts')

                @show
            <!-- Fonts END -->
            @section('css-preloaded')
                
            @show
        <!-- CSS END -->

        <!-- Begin Compatibility Zone -->
            {{--  
                @PARTIAL@ IE 8- support...
                (or for other browsers that don't support HTML5..)
                
                HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries.
                WARNING: Respond.js doesn't work if you view the page via file:// .
            --}}

            <!--[if lt IE 9]>
            @if (!Functions::testVar($usingCDNs2))
                <script src="{{ asset('js/compatibility.js') }}"></script>  
            @else
                <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
                <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
            @endif
            <![endif]-->
        <!-- End Compatibility Zone -->

        <!-- Preloaded JS START... -->
            @section('js-preloaded')

            @show
        <!-- Preloaded JS END -->


    </head>
    <body data-spy="scroll" data-target="#template-navbar">

        <header>
            <!--== 4. Navigation ==-->
            @component('components.navbar')
                @slot('useVue')
                    {!! Functions::toBladableContent('true') !!}
                @endslot
            @endcomponent
        </header>

        @section('modals')
            
        @show

        <main>
            <div id="alerts"></div>

            @section('content')
                
            @show

        </main>


        <footer>
            @component('components.footer')
                
            @endcomponent
        </footer>

        @section('css-defered')
        @show 

        <!-- <a href="#header" id="back-to-top" class="top"><i class="fa fa-chevron-up"></i></a> -->

        @section('js-main')
        @show

        @section('js-defered')
        @show
        
        {{-- 
            Some views need an extra script tag or more,
             put them in this ('js-extra') _Blade:_Section 
             in the extending view. 
        --}}
        @section('js-extra')
        @show
 
    </body>
</html>