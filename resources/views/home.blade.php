@extends('template')

@section('js-extra')
    @php
        use \App\Utilities\Functions;
        
        $logos2 = Functions::getVar(
            $site['logo']??'',
            [
                'main' => [
                    'img' => asset('lib/assets/images/Logo_main.png'),
                    'alt' => 'the Site Logo',
                ],
                'stick' => [
                    'img' => asset('lib/assets/images/Logo_stick.png'),
                    'alt' => 'the Site Logo',
                ],
            ]
        );

        $alerts2 = Functions::getVar($alerts??'', []);

        $gallery2 = Functions::getVar(
            $page['gallery']??'', 
            [
                'sortings' => ['breakfast', 'special', 'desert', 'dinner'],
                'title' => 'My Project Gallery',
                'items' => [
                    [
                        'sortings' => ['dinner'],
                        'url' => '#',
                        'img' => [
                            'img' => 'lib/assets/images/food1.jpg',
                            'alt' => 'Food',
                            'cap' => '',
                        ],
                        'title' => 'Tomato Curry',
                        'content' => 'Natalie &amp; Justin Cleaning by Justin Younger',
                        'summary' => 'Natalie &amp; Justin Cleaning by Justin Younger',
                    ],
                    [
                        'sortings' => ['breakfast'],
                        'url' => '#',
                        'img' => [
                            'img' => 'lib/assets/images/food2.jpg',
                            'alt' => 'Food',
                            'cap' => '',
                        ],
                        'title' => 'Prawn Dish',
                        'content' => 'Lorem ipsum dolor sit amet',
                        'summary' => 'Lorem ipsum dolor sit amet',
                    ],
                    [
                        'sortings' => ['desert'],
                        'url' => '#',
                        'img' => [
                            'img' => 'lib/assets/images/food3.jpg',
                            'alt' => 'Food',
                            'cap' => '',
                        ],
                        'title' => 'Salad Dish',
                        'content' => 'Consectetur adipisicing elit, sed do eiusmod',
                        'summary' => 'Consectetur adipisicing elit, sed do eiusmod',
                    ],
                    [
                        'sortings' => ['breakfast', 'special'],
                        'url' => '#',
                        'img' => [
                            'img' => 'lib/assets/images/food4.jpg',
                            'alt' => 'Food',
                            'cap' => '',
                        ],
                        'title' => 'Prawn Dish',
                        'content' => 'Tempor incididunt ut labore et dolore',
                        'summary' => 'Tempor incididunt ut labore et dolore',
                    ],
                    [
                        'sortings' => ['breakfast'],
                        'url' => '#',
                        'img' => [
                            'img' => 'lib/assets/images/food5.jpg',
                            'alt' => 'Food',
                            'cap' => '',
                        ],
                        'title' => 'Vegetable Dish',
                        'content' => 'Magna aliqua. Ut enim ad minim veniam',
                        'summary' => 'Magna aliqua. Ut enim ad minim veniam',
                    ],
                    [
                        'sortings' => ['dinner', 'special'],
                        'url' => '#',
                        'img' => [
                            'img' => 'lib/assets/images/food6.jpg',
                            'alt' => 'Food',
                            'cap' => '',
                        ],
                        'title' => 'Chicken Dish',
                        'content' => 'Quis nostrud exercitation ullamco laboris',
                        'summary' => 'Quis nostrud exercitation ullamco laboris',
                    ],
                    [
                        'sortings' => ['desert'],
                        'url' => '#',
                        'img' => [
                            'img' => 'lib/assets/images/food7.jpg',
                            'alt' => 'Food',
                            'cap' => '',
                        ],
                        'title' => 'Vegetable Noodles',
                        'content' => 'Nisi ut aliquip ex ea commodo',
                        'summary' => 'Nisi ut aliquip ex ea commodo',
                    ],
                    [
                        'sortings' => ['dinner'],
                        'url' => '#',
                        'img' => [
                            'img' => 'lib/assets/images/food8.jpg',
                            'alt' => 'Food',
                            'cap' => '',
                        ],
                        'title' => 'Special Salad',
                        'content' => 'Duis aute irure dolor in reprehenderit',
                        'summary' => 'Duis aute irure dolor in reprehenderit',
                    ],
                    [
                        'sortings' => ['desert', 'special'],
                        'url' => '#',
                        'img' => [
                            'img' => 'lib/assets/images/food9.jpg',
                            'alt' => 'Food',
                            'cap' => '',
                        ],
                        'title' => 'Ice-cream',
                        'content' => 'Excepteur sint occaecat cupidatat non',
                        'summary' => 'Excepteur sint occaecat cupidatat non',
                    ]
                ],
            ]
        );

        $about2 = Functions::getVar($page['about']??'', []);

        $entry2 = Functions::getVar($page['entries']??'', []);

        $csrf = [
            'csrf' => csrf_token(),
            'nut' => Functions::getVar($site['nut']??'', '')
        ];

        $routes = [
            'baseUrl' => url(''),
            'login' => route('login'),
            'reset' => route('password.email'),
            'contact' => route('contact'),
        ];

        $contactInfo2 = [
            'user' => [
                'name' => 'Shlomo Kallner',
                'phone' => '+972 522976373',
                'email' => 'shlomo.kallner@gmail.com',
            ],
            'socialInfo' => [
                ['url' => url('https://www.facebook.com/shlomo.kallner'), 'type' => 'fb'],
                ['url' => url('https://twitter.com/shlomo_kallner'), 'type' => 'twit'],
                ['url' => url('https://www.linkedin.com/in/shlomo-kallner-4a6730a4'), 'type' => 'link']
            ]
        ];
        
    @endphp
    <script>
        window.Laravel.resetData (
            '@json($routes)', 
            '@json($csrf)', 
            '@json($logos2)', 
            '@json($alerts2)', 
            '@json($gallery2)',
            '@json($contactInfo2)',
            50
        );
    </script>
@endsection