



<div class="modal fade" id="loginModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Login</h4>
            </div>
            <div class="modal-body">
                <div class="panel-group" id="accordionLoginModal" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div id="collapseLoginModalForm" class="panel-collapse collapse in" role="tabpanel">
                            <div class="panel-body">

                                <form id="loginModalForm" class="form-horizontal" action="{{ route('login') }}" method="POST" role="form">
                                    {{ csrf_field() }}
                
                                    <div class="form-group">
                                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>
                
                                        <div class="col-md-6">
                                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                
                                            <span id="loginModalForm-email-error" class="help-block"></span>
                                        </div>
                                    </div>
                
                                    <div class="form-group">
                                        <label for="password" class="col-md-4 control-label">Password</label>
                
                                        <div class="col-md-6">
                                            <input id="password" type="password" class="form-control" name="password" required>
                
                                            <span id="loginModalForm-password-error" class="help-block"></span>
                                        </div>
                                    </div>
                
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                
                                    <div id="loginModalFormResponse"></div>
                
                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-4">
                                            <button id="loginModalFormSubmit" type="submit" class="btn btn-primary">
                                                Login
                                            </button>
                
                                            <button  type="button" class="btn btn-link" role="button" data-toggle="collapse" data-parent="#accordionLoginModal"
                                                data-target="#collapseLoginModalRequest" aria-expanded="false" aria-controls="collapseLoginModalRequest">
                                                Forgot Your Password?
                                            </button>
                                        </div>
                                    </div>
                
                                </form>

                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                
                        <div id="collapseLoginModalRequest" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                
                                <form id="loginModalRequestForm" class="form-horizontal" action="{{ route('password.email') }}" method="POST" role="form">
                                    {{ csrf_field() }}
        
                                    <div class="form-group">
                                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>
        
                                        <div class="col-md-6">
                                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
        
                                            <span id="loginModalRequestForm-email-error" class="help-block"></span>
        
                                        </div>
                                    </div>
        
                                    <div id="loginModalRequestFormResponse"></div>
        
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button id="loginModalRequestFormSubmit" type="submit" class="btn btn-primary">
                                                Send Password Reset Link
                                            </button>
                
                                            <button  type="button" class="btn btn-link" role="button" data-toggle="collapse" data-parent="#accordionLoginModal"
                                                data-target="#collapseLoginModalForm" aria-expanded="false" aria-controls="collapseLoginModalForm">
                                                Want to Try Your Password Again?
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
