@extends('master')

@include('inc.css')
@include('inc.js')

@section('modals')
    @parent
        
    @if (false)
        @component('auth.login_modal')
            
        @endcomponent
    @endif
    
@endsection

@section('content')

    @php
        use \App\Utilities\Functions;

        $testing = true;
        $usingVue = true;

        $entryItems2 = Functions::getVar($page['entries']['items']??'', []);
        $entryImage2 = Functions::getVar($page['entries']['img']??'', []);
        $aboutItems2 = Functions::getVar($page['about']['items']??'', []);
        $aboutImage2 = Functions::getVar($page['about']['img']??'', []);
        $galleryItems2 = Functions::getVar($page['gallery']['items']??'', []);
        $gallerySortings2 = Functions::getVar($page['gallery']['sortings']??'', []);
        $galleryTitle2 = Functions::getVar($page['gallery']['title']??'', 'My Project Gallery');

    @endphp

    <!--== 5. Header/Entry Point ==-->
    @if (false)
        <div id="entry"></div>
    @else
        @component('components.entry')
            @slot('items')
                {!! Functions::toBladableContent($entryItems2) !!}
            @endslot
            @slot('img')
                {!! Functions::toBladableContent($entryImage2) !!}
            @endslot
        @endcomponent
    @endif

    <!--== 6. About us ==-->
    @if (false)
        <div id="about"></div>
    @else
        @component('components.about')
            @slot('items')
                {!! Functions::toBladableContent($aboutItems2) !!}
            @endslot
            @slot('img')
                {!! Functions::toBladableContent($aboutImage2) !!}
            @endslot
        @endcomponent
    @endif

    <!--==  7. Gallery  ==-->
    @if ($usingVue)
        <div id="gallery"></div>
    @else
        @component('components.gallery')
            @slot('title')
                {!! Functions::toBladableContent($galleryTitle2) !!}
            @endslot
            @slot('items')
                {!! Functions::toBladableContent($galleryItems2) !!}
            @endslot
            @slot('sortings')
                {!! Functions::toBladableContent($gallerySortings2) !!}
            @endslot
        @endcomponent
    @endif
    
    @component('components.contact')
        
    @endcomponent

@endsection