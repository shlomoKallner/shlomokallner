@php
    use \App\Utilities\Functions;

    $testing = false;

    $items2 = Functions::getUnBladedContent($items??'', []);
    $title2 = Functions::getBladedString($title??'', 'Affordable Pricing');
    $sortings2 = Functions::getUnBladedContent($sortings??'', []);
    
    if (!$testing && !Functions::countHas($sortings2)) {
        $sortings2[] = 'breakfast';
        $sortings2[] = 'special';
        $sortings2[] = 'desert';
        $sortings2[] = 'dinner';
    }

    if (!$testing && !Functions::countHas($items2)) {
        $items2[] = [
            'sortings' => ['dinner'],
            'url' => '#',
            'img' => [
                'img' => 'lib/assets/images/food1.jpg',
                'alt' => 'Food'
            ],
            'title' => 'Tomato Curry',
            'content' => 'Natalie &amp; Justin Cleaning by Justin Younger',
        ];
        $items2[] = [
            'sortings' => ['breakfast'],
            'url' => '#',
            'img' => [
                'img' => 'lib/assets/images/food2.jpg',
                'alt' => 'Food'
            ],
            'title' => 'Prawn Dish',
            'content' => 'Lorem ipsum dolor sit amet',
        ];
        $items2[] = [
            'sortings' => ['desert'],
            'url' => '#',
            'img' => [
                'img' => 'lib/assets/images/food3.jpg',
                'alt' => 'Food'
            ],
            'title' => 'Salad Dish',
            'content' => 'Consectetur adipisicing elit, sed do eiusmod',
        ];
        $items2[] = [
            'sortings' => ['breakfast', 'special'],
            'url' => '#',
            'img' => [
                'img' => 'lib/assets/images/food4.jpg',
                'alt' => 'Food'
            ],
            'title' => 'Prawn Dish',
            'content' => 'Tempor incididunt ut labore et dolore',
        ];
        $items2[] = [
            'sortings' => ['breakfast'],
            'url' => '#',
            'img' => [
                'img' => 'lib/assets/images/food5.jpg',
                'alt' => 'Food'
            ],
            'title' => 'Vegetable Dish',
            'content' => 'Magna aliqua. Ut enim ad minim veniam',
        ];
        $items2[] = [
            'sortings' => ['dinner', 'special'],
            'url' => '#',
            'img' => [
                'img' => 'lib/assets/images/food6.jpg',
                'alt' => 'Food'
            ],
            'title' => 'Chicken Dish',
            'content' => 'Quis nostrud exercitation ullamco laboris',
        ];
        $items2[] = [
            'sortings' => ['desert'],
            'url' => '#',
            'img' => [
                'img' => 'lib/assets/images/food7.jpg',
                'alt' => 'Food'
            ],
            'title' => 'Vegetable Noodles',
            'content' => 'Nisi ut aliquip ex ea commodo',
        ];
        $items2[] = [
            'sortings' => ['dinner'],
            'url' => '#',
            'img' => [
                'img' => 'lib/assets/images/food8.jpg',
                'alt' => 'Food'
            ],
            'title' => 'Special Salad',
            'content' => 'Duis aute irure dolor in reprehenderit',
        ];
        $items2[] = [
            'sortings' => ['desert', 'special'],
            'url' => '#',
            'img' => [
                'img' => 'lib/assets/images/food9.jpg',
                'alt' => 'Food'
            ],
            'title' => 'Ice-cream',
            'content' => 'Excepteur sint occaecat cupidatat non',
        ];
    }
@endphp

<section id="pricing" class="pricing">
    <div id="w">
        <div class="pricing-filter">
            <div class="pricing-filter-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="section-header">
                                <h2 class="pricing-title">{{ $title2 }}</h2>
                                <ul id="filter-list" class="clearfix">
                                    <li class="filter" data-filter="all">All</li>
                                    @if (Functions::countHas($sortings2))
                                        @foreach ($sortings2 as $item)
                                            <li class="filter" data-filter=".{{ $item }}">{{ title_case($item) }}</li>
                                        @endforeach
                                    @elseif ($testing)
                                        <li class="filter" data-filter=".breakfast">Breakfast</li>
                                        <li class="filter" data-filter=".special">Special</li>
                                        <li class="filter" data-filter=".desert">Desert</li>
                                        <li class="filter" data-filter=".dinner">Dinner</li>
                                    @endif
                                </ul><!-- @end #filter-list -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">  
                <div class="col-md-10 col-md-offset-1">
                    <ul id="menu-pricing" class="menu-price">
                        @if (Functions::countHas($items2))
                            @foreach ($items2 as $item)
                                @component('components.gallery_item')
                                    @foreach ($item as $key => $val)
                                        @slot($key)
                                            {!! Functions::toBladableContent($val) !!}
                                        @endslot
                                    @endforeach
                                @endcomponent
                            @endforeach
                        @elseif ($testing)
                            <li class="item dinner">

                                <a href="#">
                                    <img src="{{ asset('lib/assets/images/food1.jpg')}}" class="img-responsive" alt="Food" >
                                    <div class="menu-desc text-center">
                                        <span>
                                            <h3>Tomato Curry</h3>
                                            Natalie &amp; Justin Cleaning by Justin Younger
                                        </span>
                                    </div>
                                </a>
                                    
                                <h2 class="white">$20</h2>
                            </li>
                            <li class="item breakfast">

                                <a href="#">
                                    <img src="{{ asset('lib/assets/images/food2.jpg')}}" class="img-responsive" alt="Food" >
                                    <div class="menu-desc">
                                        <span>
                                            <h3>Prawn Dish</h3>
                                            Lorem ipsum dolor sit amet
                                        </span>
                                    </div>
                                </a>
                                    
                                <h2 class="white">$20</h2>
                            </li>
                            <li class="item desert">

                                <a href="#">
                                    <img src="{{ asset('lib/assets/images/food3.jpg')}}" class="img-responsive" alt="Food" >
                                    <div class="menu-desc">
                                        <span>
                                            <h3>Salad Dish</h3>
                                            Consectetur adipisicing elit, sed do eiusmod
                                        </span>
                                    </div>
                                </a>
                                    
                                <h2 class="white">$18</h2>
                            </li>
                            <li class="item breakfast special">

                                <a href="#">
                                    <img src="{{ asset('lib/assets/images/food4.jpg')}}" class="img-responsive" alt="Food" >
                                    <div class="menu-desc">
                                        <span>
                                            <h3>Prawn Dish</h3>
                                            Tempor incididunt ut labore et dolore
                                        </span>
                                    </div>
                                </a>
                                    
                                <h2 class="white">$15</h2>
                            </li>
                            <li class="item breakfast">

                                <a href="#">
                                    <img src="{{ asset('lib/assets/images/food5.jpg')}}" class="img-responsive" alt="Food" >
                                    <div class="menu-desc">
                                        <span>
                                            <h3>Vegetable Dish</h3>
                                            Magna aliqua. Ut enim ad minim veniam
                                        </span>
                                    </div>
                                </a>
                                    
                                <h2 class="white">$20</h2>
                            </li>
                            <li class="item dinner special">

                                <a href="#">
                                    <img src="{{ asset('lib/assets/images/food6.jpg')}}" class="img-responsive" alt="Food" >
                                    <div class="menu-desc">
                                        <span>
                                            <h3>Chicken Dish</h3>
                                            Quis nostrud exercitation ullamco laboris
                                        </span>
                                    </div>
                                </a>

                                <h2 class="white">$22</h2>
                            </li>
                            <li class="item desert">

                                <a href="#">
                                    <img src="{{ asset('lib/assets/images/food7.jpg')}}" class="img-responsive" alt="Food" >
                                    <div class="menu-desc">
                                        <span>
                                            <h3>Vegetable Noodles</h3>
                                            Nisi ut aliquip ex ea commodo
                                        </span>
                                    </div>
                                </a>

                                <h2 class="white">$32</h2>
                            </li>
                            <li class="item dinner">

                                <a href="#">
                                    <img src="{{ asset('lib/assets/images/food8.jpg')}}" class="img-responsive" alt="Food" >
                                    <div class="menu-desc">
                                        <span>
                                            <h3>Special Salad</h3>
                                            Duis aute irure dolor in reprehenderit
                                        </span>
                                    </div>
                                </a>

                                <h2 class="white">$38</h2>
                            </li>
                            <li class="item desert special">

                                <a href="#">
                                    <img src="{{ asset('lib/assets/images/food9.jpg')}}" class="img-responsive" alt="Food" >
                                    <div class="menu-desc">
                                        <span>
                                            <h3>Ice-cream</h3>
                                            Excepteur sint occaecat cupidatat non
                                        </span>
                                    </div>
                                </a>
                                
                                <h2 class="white">$38</h2>
                            </li> 
                        @endif 
                    </ul>

                    <!-- 
                        <div class="text-center">
                            <a id="loadPricingContent" class="btn btn-middle hidden-sm hidden-xs">Load More <span class="caret"></span></a>
                        </div> 
                    -->

                </div>   
            </div>
        </div>

    </div> 
</section>