

@php
    use \App\Utilities\Functions;

    $testing = false;

    $title2 = Functions::getBladedString($title??'', '');
    $subTitle2 = Functions::getBladedString($subTitle??'', '');
    $extraCss2 = Functions::getBladedString($extraCss??'', '');
    $img2 = Functions::getUnBladedContent($img??'', []);
    $pos2 = Functions::getBladedString($pos??'', '0% 0%'); 

@endphp

@if ($testing)

    <div class="item">
        <div class="container">
            <div class="header-content">
                <h1 class="header-title">BEST FOOD</h1>
                <p class="header-sub-title">create your own slogan</p>
            </div> <!-- /.header-content -->
        </div>
    </div>

@else

    <div class="item has-img" style="background-image: url({{ asset($img2['img']) }}); background-position: {{ $pos2 }};">
        <div class="container">
            <div class="header-content{{ Functions::testVar($extraCss2) ? ' ' . $extraCss2 : '' }}">
                <h1 class="header-title">{{ $title2 }}</h1>
                <p class="header-sub-title">{{ $subTitle2 }}</p>
            </div> <!-- /.header-content -->
        </div>
    </div>

@endif
