
@php
    $testing = false;
    $useVue = true;
@endphp


<section id="contact" class="contact">
    @if ($useVue || $testing)
        <div id="contactInfo"></div>
    @else
        <div id="contactInfo" class="container-fluid color-bg">
            <div class="row dis-table">
                <div class="hidden-xs col-sm-6 dis-table-cell">
                    <h2 class="section-title">Contact Me</h2>
                </div>
                <div class="col-xs-6 col-sm-6 dis-table-cell">
                    <div class="section-content">
                        <p>Shlomo Kallner</p>
                        <p>+972 522976373</p>
                        <p>shlomo.kallner@gmail.com</p>
                    </div>
                </div>
            </div>
            <div class="social-media">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <ul class="center-block">
                            <li><a href="{{ url('https://www.facebook.com/shlomo.kallner') }}" class="fb"></a></li>
                            <li><a href="{{ url('https://twitter.com/shlomo_kallner') }}" class="twit"></a></li>
                            {{-- <li><a href="#" class="g-plus"></a></li> --}}
                            <li><a href="{{ url('https://www.linkedin.com/in/shlomo-kallner-4a6730a4') }}" class="link"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    @endif
</section>



<section class="contact-form">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
                @if ($useVue || $testing)
                    <div id="contactFormContainer"></div>
                @else
                    <div class="row" id="contactFormContainer">
                        <form id="contactForm" class="contact-form" method="POST" action="{{ url('contact') }}">
                            {{ csrf_field() }}    
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <input name="name" type="text" class="form-control" id="name" required="required" placeholder="  Name">
                                    <span id="contactForm-name-error" class="help-block"></span>
                                </div>
                                <div class="form-group">
                                    <input name="email" type="email" class="form-control" id="email" required="required" placeholder="  Email">
                                    <span id="contactForm-email-error" class="help-block"></span>
                                </div>
                                <div class="form-group">
                                    <input name="phone" type="tel" class="form-control" id="phone" required="required" placeholder="  Phone">
                                    <span id="contactForm-phone-error" class="help-block"></span>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <textarea name="message" type="text" class="form-control" id="message" rows="8" required="required" placeholder="  Message"></textarea>
                                    <span id="contactForm-message-error" class="help-block"></span>
                                </div>
                            </div>
                            <div id="contactFormResponse"></div>
                            <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
                                <div class="text-center">
                                    <button type="submit" id="cfsubmit" class="btn btn-send">
                                        Contact Me
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                @endif
            </div>
        </div>
    </div>
</section>