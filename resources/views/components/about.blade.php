
@php
    use \App\Utilities\Functions;

    $testing = false;

    $items2 = Functions::getUnBladedContent($items??'', []);
    $img2 = Functions::getUnBladedContent(
        $img??'', [
            'img' => 'lib/assets/images/About-C-bg.jpg',
        ]
    );
    $title2 = Functions::getUnBladedContent($title??'', 'About us');

    if (!$testing && !Functions::countHas($items2)) {
        $items2[] = [
            'text' => 'Astronomy compels the soul to look upward, and leads us from this world to another.  Curious that we spend more time congratulating people who have succeeded than encouraging people who have not. As we got further and further away, it [the Earth] diminished in size.',
            'extraCss' => '',
        ];
        $items2[] = [
            'text' => 'beautiful, warm, living object looked so fragile, so delicate, that if you touched it with a finger it would crumble and fall apart. Seeing this has to change a man.  Where ignorance lurks, so too do the frontiers of discovery and imagination.',
            'extraCss' => '',
        ];
        $items2[] = [
            'text' => 'BEST FOOD BEST SNACKS BEST DRINKS',
            'extraCss' => 'bg-info',
        ];
    }
@endphp

<section id="about" class="about">
    <img src="{{asset('lib/assets/images/icons/about_color.png')}}" class="img-responsive section-icon hidden-sm hidden-xs">
    <div class="wrapper">
        <div class="container-fluid">
            @if (false)
                <div id="aboutcomp"></div>
            @else
                <div class="row dis-table">
                    <div class="hidden-xs col-sm-6 section-bg about-bg dis-table-cell" style="background-image: url({{ asset($img2['img']) }});">

                    </div>
                    <div class="col-xs-12 col-sm-6 dis-table-cell">
                        <div class="section-content">
                            <h2 class="section-content-title">{{ $title2 }}</h2>
                            @if (Functions::countHas($items2))

                                @foreach ($items2 as $item)
                                    <p class="section-content-para{{ Functions::testVar($item['extraCss']) ? ' ' . $item['extraCss'] : '' }}">
                                        {!! $item['text'] !!}
                                    </p>
                                @endforeach
                            
                            @elseif ($testing)
                                <p class="section-content-para">
                                    Astronomy compels the soul to look upward, and leads us from this world to another.  Curious that we spend more time congratulating people who have succeeded than encouraging people who have not. As we got further and further away, it [the Earth] diminished in size.
                                </p>
                                <p class="section-content-para">
                                    beautiful, warm, living object looked so fragile, so delicate, that if you touched it with a finger it would crumble and fall apart. Seeing this has to change a man.  Where ignorance lurks, so too do the frontiers of discovery and imagination.
                                </p>
                            @endif
                        </div> <!-- /.section-content -->
                    </div>
                </div> <!-- /.row -->
            @endif
        </div> <!-- /.container-fluid -->
    </div> <!-- /.wrapper -->
</section> <!-- /#about -->