@php
    use \App\Utilities\Functions;

    $testing = false;

    $sortings2 = Functions::getUnBladedContent($sortings??'', ['dinner']);
    $url2 = Functions::getBladedString($url??'', '#');
    $img2 = Functions::getUnBladedContent($img??'', []);
    $title2 = Functions::getBladedString($title??'', '');
    $content2 = Functions::getBladedString($content??'', '');

@endphp

    @if ($testing)

        <li class="item dinner">

            <a href="#">
                <img src="{{ asset('lib/assets/images/food1.jpg')}}" class="img-responsive" alt="Food" >
                <div class="menu-desc text-center">
                    <span>
                        <h3>Tomato Curry</h3>
                        Natalie &amp; Justin Cleaning by Justin Younger
                    </span>
                </div>
            </a>
                
            <h2 class="white">$20</h2>
        </li>

    @else

        <li class="item {{ implode(' ', $sortings2) }}">

            <a href="{{ url($url2) }}">
                <img src="{{ asset($img2['img']) }}" class="img-responsive" alt="{{ $img2['alt'] }}" >
                <div class="menu-desc text-center">
                    <span>
                        <h3>{{ $title2 }}</h3>
                        {{ $content2 }}
                    </span>
                </div>
            </a>
                
            <h2 class="white">{{ $title2 }}</h2>
        </li>

    @endif