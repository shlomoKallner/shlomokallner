

@php
    use \App\Utilities\Functions;

    $testing = false;

    $items2 = Functions::getUnBladedContent($items??'', []);
    
    if (!$testing && !Functions::countHas($items2)) {
        $items2[] = [
            'title' => 'BEST FOOD',
            'subTitle' => 'create your own slogan',
            'extraCss' => '',
            'img' => [
                'img' => 'lib/assets/images/1.jpg',
            ],
            'pos' => 'bottom',
        ];
        $items2[] = [
            'title' => 'BEST SNACKS',
            'subTitle' => 'create your own slogan',
            'extraCss' => '',
            'img' => [
                'img' => 'lib/assets/images/2.jpg',
            ],
            'pos' => '',
        ];
        $items2[] = [
            'title' => 'BEST DRINKS',
            'subTitle' => 'create your own slogan',
            'extraCss' => 'text-right pull-right',
            'img' => [
                'img' => 'lib/assets/images/3.jpg',
            ],
            'pos' => '',
        ];
    }

@endphp

<section id="header-slider" class="owl-carousel">
    @if ($testing)
        <div class="item">
            <div class="container">
                <div class="header-content">
                    <h1 class="header-title">BEST FOOD</h1>
                    <p class="header-sub-title">create your own slogan</p>
                </div> <!-- /.header-content -->
            </div>
        </div>
        <div class="item">
            <div class="container">
                <div class="header-content">
                    <h1 class="header-title">BEST SNACKS</h1>
                    <p class="header-sub-title">create your own slogan</p>
                </div> <!-- /.header-content -->
            </div>
        </div>
        <div class="item">
            <div class="container">
                <div class="header-content text-right pull-right">
                    <h1 class="header-title">BEST DRINKS</h1>
                    <p class="header-sub-title">create your own slogan</p>
                </div> <!-- /.header-content -->
            </div>
        </div>
    @elseif (Functions::countHas($items2))
        @foreach ($items2 as $item)
            @component('components.entry_slogan')
                @foreach ($item as $key => $val)
                    @slot($key)
                        {!! Functions::toBladableContent($val) !!}
                    @endslot
                @endforeach
            @endcomponent
        @endforeach
    @endif
</section>