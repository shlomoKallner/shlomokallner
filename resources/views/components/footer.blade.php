
@php
    use \App\Utilities\Functions;

    $testing = false;

    $siteName2 = Functions::getVar($siteName??'', 'Shlomo Kallner');
@endphp

<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="copyright text-center">
                <p>
                    &copy; Copyright, {{ date('Y') }} <a href="#">{{ $siteName2 }}.</a> Theme by <a href="http://themewagon.com/"  target="_blank">ThemeWagon</a>
                </p>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3">
            <p class="copyright text-center">
                Powered by:
                <a class="" href="http://htmlpurifier.org/">
                    <img src="{{ asset('images/site/poweredByHtmlPurifier.png') }}"
                        alt="Powered by HTML Purifier" border="0" />
                </a> 
                {{-- 
                    <br>
                    From: <a href="http://htmlpurifier.org/">HTML Purifier</a>
                --}}
            </p>
        </div>
    </div>
    
</div>