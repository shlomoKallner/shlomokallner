
@php
    use \App\Utilities\Functions;

    $img2 = Functions::getUnBladedContent(
        $img??'', [
            'img' => 'lib/assets/images/Logo_main.png',
            'alt' => 'the Site Logo',
        ]
    );

    $useVue2 = Functions::getVar($useVue??'', '');

    $currentCV2 = Functions::getVar($currentCV??'', 'docs/shlomoKallner.pdf');

@endphp

<nav id="template-navbar" class="navbar navbar-default custom-navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#Food-fair-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            @if (Functions::testVar($useVue2))
                <div id="logo"></div>
            @else
                <a class="navbar-brand" href="{{ url('') }}">
                    <img id="logo" src="{{ asset($img2['img'])}}" alt="{{ $img2['alt'] }}" class="logo img-responsive">                
                </a>
            @endif
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="Food-fair-toggle">
            
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav navbar-left">
                <li><a href="#about">about</a></li>
                <li><a href="#pricing">gallery</a></li>
                {{-- 
                    <li><a href="#great-place-to-enjoy">beer</a></li>
                    <li><a href="#breakfast">bread</a></li>
                    <li><a href="#featured-dish">featured</a></li>
                    <li><a href="#reserve">reservation</a></li>
                --}}
                <li><a href="#contact">contact</a></li>
                <li><a href="{{asset($currentCV2)}}" target="_blank">My CV</a></li>
            </ul>
            
            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @guest

                    {{-- <li><a href="{{ route('login') }}">Login</a></li> --}}
                    {{-- <li><a href="{{ route('register') }}">Register</a></li> --}}
                    @if (false)
                    
                        <li>
                            <a data-toggle="modal" data-target="#loginModal">Login</a>
                        </li>
                        
                        
                    @else
                    
                        <li id="login"></li>

                    @endif
                    
                    
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li>
                                <a data-toggle="modal" data-target="#headerImage">Add or Remove a Header Image</a>
                            </li>
                            <li>
                                <a data-toggle="modal" data-target="#profileInfo">Modify Profile Information</a>
                            </li>
                            <li>
                                <a data-toggle="modal" data-target="#profileInfo">Modify Contact Information</a>
                            </li>
                            <li>
                                <a data-toggle="modal" data-target="#portfolioProject">Add a Portfolio Project</a>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li>
                                {{-- 
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>
                                --}}
                                
                                <form class="form-inline" id="logout-form" action="{{ route('logout') }}" method="POST" role="form">
                                    {{ csrf_field() }}
                                    
                                    <button type="submit" name="submit" class="btn btn-primary">Logout</button>
                                    
                                </form>
                                
                            </li>
                        </ul>
                    </li>
                @endguest
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.row -->
</nav>
        