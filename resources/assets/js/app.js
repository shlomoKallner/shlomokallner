
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import './bootstrap';


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import Gallery from './components/Gallery.vue';
import Login from './components/Login.vue';
import ContactForm from './components/ContactForm.vue'
import ContactInfo from './components/ContactInfo.vue'

import createCommonComponents from './laravel';

import LaravelObject from './lib/LaravelObject';
import LaravelStore from './store/LaravelStore';

window.Vue.component('gallery-component', Gallery);
window.Vue.component('login-component', Login);
window.Vue.component('contact-form-component', ContactForm);
window.Vue.component('contact-info-component', ContactInfo);
let debug = true;
let store = new LaravelStore(window.axios, debug);

let {logos, alerts, gallery} = createCommonComponents(window, window.Vue, store);
// let login = new window.Vue(
//   {
//     el: '#login',
//     template: `<login-component :loginRoute="loginRoute" :resetRoute="resetRoute"></login-component>`,
//     store,
//     data() {
//       return {
//         loginRoute: store.getters['Urls/url']('login'),
//         resetRoute: store.getters['Urls/url']('reset')
//       }
//     },
//     watch: {
//       '$store.state.Urls._urlMap': function (val, old) {
//         if (this.$store.getters['Debug/debug']) {
//           console.log('in app.js { login->watch() } ->', [val, old, this.loginRoute, this.resetRoute]);
//         }
//         if (this.loginRoute === '' && val.has('login')) {
//           let s = val.get('login');
//           this.loginRoute = s !== '' ? s : this.loginRoute;
//         }
//         if (this.resetRoute === '' && val.has('reset')) {
//           let s = val.get('reset');
//           this.resetRoute = s !== '' ? s : this.resetRoute;
//         }
//         if (this.$store.getters['Debug/debug']) {
//           console.log('in app.js { login->watch() } ->', [val, old, this.loginRoute, this.resetRoute]);
//         }
//       }
//     },
//   }
// );
// let contactForm = new window.Vue(
//   {
//     el: '#contactFormContainer',
//     template: `<></contact-form-component>`,
//     store
//   }
// );
// let contactInfo = new window.Vue(
//   {
//     el: '#contactInfo',
//     template: `<contact-info-component></contact-info-component>`,
//     store
//   }
// );

// 
window.Laravel = new LaravelObject(window.Vue, store, logos, alerts, gallery);
//window.Laravel.login = login;
//window.Laravel.contactForm = contactForm;
// window.Laravel.contactInfo = contactInfo;
window.Laravel.createContactInfo ('#contactInfo');

