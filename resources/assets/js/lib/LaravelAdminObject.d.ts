import { StoreExtendedVue, StoreExtendedVueInstance } from './types/vue';
import LaravelObject from './LaravelObject';
import BootAlert from '../components/Alert.vue';
import BootLogos from '../components/Logos.vue';
import Gallery from "../components/Gallery.vue";
import { LaravelAdminStore } from '../store';
import Vue from 'vue';
export default class LaravelAdminObject extends LaravelObject<LaravelAdminStore> {
    constructor(_vue: StoreExtendedVue<Vue, LaravelAdminStore>, _store: LaravelAdminStore, _logos: StoreExtendedVueInstance<BootLogos, LaravelAdminStore>, _alerts: StoreExtendedVueInstance<BootAlert, LaravelAdminStore>, _gallery: StoreExtendedVueInstance<Gallery, LaravelAdminStore>);
}
