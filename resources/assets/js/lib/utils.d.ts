import { CsrfObject } from './types';
export declare function toCsrf(csrf: string | CsrfObject): CsrfObject;
export declare function isValid(data: any): boolean;
export declare function title_case(str: string): string;
export declare function get_uiv_Notification_type(str: string): string;
export declare function get_uiv_Notification_custom_classes(str: string): string;
export interface PropertySetter {
    (val: any): void;
}
export interface PropertyGetter {
    (): any;
}
export declare function set_Prop_on_Object_if_undefined(obj: object, prop: string | symbol | number, value: any, writable?: boolean, enumerable?: boolean, configurable?: boolean): void;
