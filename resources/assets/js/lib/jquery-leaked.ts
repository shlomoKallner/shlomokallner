
/// based on an answer from 'https://stackoverflow.com/questions/34338411/how-to-import-jquery-using-es6-syntax'
/**
 * 

    If it helps anyone, javascript import statements are hoisted. Thus, if a library has a dependency (eg bootstrap) on jquery in the global namespace (window), this will NOT work:

    import {$,jQuery} from 'jquery';
    window.$ = $;
    window.jQuery = jQuery;
    import 'bootstrap/dist/js/bootstrap.min';

    This is because the import of bootstrap is hoisted and evaluated before jQuery is attached to window.

    One way to get around this is to not import jQuery directly, but instead import a module which itself imports jQuery AND attaches it to the window.

    import jQuery from './util/leaked-jquery';
    import 'bootstrap/dist/js/bootstrap.min';

    where leaked-jquery looks like:

    import {$,jQuery} from 'jquery';
    window.$ = $;
    window.jQuery = jQuery;
    export default $;
    export jQuery;

    EG, https://github.com/craigmichaelmartin/weather-app--birch/blob/4d9f3b03719e0a2ea3fb5ddbbfc453a10e9843c6/javascript/util/leak_jquery.js

    answered Jul 10 '18 at 18:25
    craigmichaelmartin (https://stackoverflow.com/users/3670208/craigmichaelmartin)

 */

import jQuery from 'jquery'
import { set_Prop_on_Object_if_undefined } from './utils';

set_Prop_on_Object_if_undefined(window, '$', jQuery, true, true, true);

set_Prop_on_Object_if_undefined(window, 'jQuery', jQuery, true, true, true);

export default $;
export {jQuery};