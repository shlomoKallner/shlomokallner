


import {ImageObject} from './common';


interface GalleryModalObject {

}

export interface GalleryPayloadObject {
    items?: GalleryObject[];
    sortings?: string[];
    title?: string;
}

export interface GallerySingleObject {
    item?: GalleryObject;
    sorting?: string;
}

export interface GalleryObject {
    img: ImageObject;
    url: string;
    title: string;
    summary: string;
    sortings: string[];
    content: string;
    labelCss: string;
}

export {ImageObject};