
import {ImageObject} from './common';


export interface LogosObject {
    main: ImageObject | null;
    stick: ImageObject | null;
}

export interface LogosPayloadObject {
    logos: LogosObject; 
    scrollPast: number;
}

export {ImageObject};
