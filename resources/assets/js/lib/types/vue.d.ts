


import Vue from 'vue';
import { Store, StoreOptions } from 'vuex';
import { CombinedVueInstance, ExtendedVue } from 'vue/types/vue';
import { 
    Component, ComponentOptions, 
    PropsDefinition, FunctionalComponentOptions 
} from 'vue/types/options';
import { PluginObject, PluginFunction } from 'vue/types/plugin';



export interface StoreExtenderPluginFunction<T, V extends Vue, S extends Store<any>> {
    (_vue: StoreExtendedVue<V, S>, options?: T) : void ;
}; 
export type StoreExtendedPluginFunction<T, V extends Vue, S extends Store<any>> = 
    PluginFunction<T> | StoreExtenderPluginFunction<T, V, S>;

export interface StoreExtendedPluginObject<T, V extends Vue, S extends Store<any>> extends PluginObject<T> {
    install: StoreExtendedPluginFunction<T, V, S>;
};

export type OurPropType = Record<string | keyof object, any> | Object;

export type VueInstance<
    V extends Vue, Data = object, 
    Methods = object, 
    Computed = object, 
    Props = OurPropType
> = CombinedVueInstance<V, Data, Methods, Computed, Props> | Object;

export type DefaultData<V> =  object | ((this: V) => object);
export type DefaultProps = Record<string, any>;
export type DefaultMethods<V> =  { [key: string]: (this: V, ...args: any[]) => any };
export type DefaultComputed = { [key: string]: any };
export type DataDef<Data, Props, V> = Data | ((this: Readonly<Props> & V) => Data);

export interface StoreExtendedComponentOptions<
    V extends Vue, S extends Store<any>,
    Data=DefaultData<V>,
    Methods=DefaultMethods<V>,
    Computed=DefaultComputed,
    PropsDef=PropsDefinition<OurPropType>,
    Props=OurPropType
> extends ComponentOptions<
    V, Data, Methods, Computed, PropsDef, Props
> {
    store?: S; 
    components?: { [key: string]: StoreExtendedComponent<V, S, Data, Methods, Computed, Props> | StoreExtendedAsyncComponent<V, S, any, any, any, any> };
    mixins?: (StoreExtendedComponentOptions<Vue, Store<any>> | typeof Vue)[];
    // TODO: support properly inferred 'extends'
    extends?: StoreExtendedComponentOptions<Vue, Store<any>> | typeof Vue;
};

/**
* This type should be used when an array of strings is used for a component's `props` value.
*/
export type StoreExtendedThisTypedComponentOptionsWithArrayProps<V extends Vue, S extends Store<any>, Data, Methods, Computed, PropNames extends string> =
 object &
 ThisTypedComponentOptionsWithArrayProps<V, Data, Methods, Computed, PropNames> &
 StoreExtendedComponentOptions<V, S, DataDef<Data, Record<PropNames, any>, V>, Methods, Computed, PropNames[], Record<PropNames, any>> &
 ThisType<StoreExtendedComponentOptions<V, S, Data, Methods, Computed, Readonly<Record<PropNames, any>>>>;

/**
* This type should be used when an object mapped to `PropOptions` is used for a component's `props` value.
*/
export type StoreExtendedThisTypedComponentOptionsWithRecordProps<V extends Vue, S extends Store<any>, Data, Methods, Computed, Props> =
 object &
 ThisTypedComponentOptionsWithRecordProps<V, Data, Methods, Computed, Props> &
 StoreExtendedComponentOptions<V, S, DataDef<Data, Props, V>, Methods, Computed, RecordPropsDefinition<Props>, Props> &
 ThisType<StoreExtendedComponentOptions<V, S, Data, Methods, Computed, Readonly<Props>>>;

export interface StoreExtendedVueInstance<V extends Vue, S extends Store, 
Data = object, Methods = object, Computed = object, Props = OurPropType
> extends VueInstance<V, Data, Methods, Computed, Props> {
    $store: S;
    readonly $options: StoreExtendedComponentOptions<V, S, Data, Methods, Computed, PropsDefinition<Props>, Props>;
}

export interface StoreExtendedVue<V extends Vue, S extends Store<any>, 
    Data = object, Methods = object, Computed = object, Props = OurPropType
> extends ExtendedVue<V, Data, Methods, Computed, Props> {
    new <V1 extends V = V, S1 extends S = S, Data = object, Methods = object, Computed = object, PropNames extends string = never>(options?: StoreExtendedThisTypedComponentOptionsWithArrayProps<V1, S1, Data, Methods, Computed, PropNames>): StoreExtendedVueInstance<V1, S1, Data, Methods, Computed, Record<PropNames, any>>;
    // ideally, the return type should just contain Props, not Record<keyof Props, any>. But TS requires to have Base constructors with the same return type.
    new <V1 extends V = V, S1 extends S = S, Data = object, Methods = object, Computed = object, Props = object>(options?: StoreExtendedThisTypedComponentOptionsWithRecordProps<V1, S1, Data, Methods, Computed, Props>): StoreExtendedVueInstance<V1, S1, Data, Methods, Computed, Record<keyof Props, any>>;
    new <V1 extends V = V, S1 extends S = S>(options?: StoreExtendedComponentOptions<V1, S1>): StoreExtendedVueInstance<V1, S1, object, object, object, Record<keyof object, any>>;
  
    extend<V1 extends Vue = V, S1 extends Store<any> = S, Data, Methods, Computed, PropNames extends string = never>(options?: StoreExtendedThisTypedComponentOptionsWithArrayProps<V1, S1, Data, Methods, Computed, PropNames>): StoreExtendedVue<V1, S1, Data, Methods, Computed, Record<PropNames, any>>;
    extend<V1 extends Vue = V, S1 extends Store<any> = S, Data, Methods, Computed, Props>(options?: StoreExtendedThisTypedComponentOptionsWithRecordProps<V1, S1, Data, Methods, Computed, Props>): StoreExtendedVue<V1, S1, Data, Methods, Computed, Props>;
    extend<V1 extends Vue = V, S1 extends Store<any> = S, PropNames extends string = never>(definition: StoreExtendedFunctionalComponentOptions<S1, Record<PropNames, any>, PropNames[]>): StoreExtendedVue<V1, S1, {}, {}, {}, Record<PropNames, any>>;
    extend<V1 extends Vue = V, S1 extends Store<any> = S, Props = OurPropType>(definition: StoreExtendedFunctionalComponentOptions<S1, Props, RecordPropsDefinition<Props>>): StoreExtendedVue<V1, S1, {}, {}, {}, Props>;
    extend<V1 extends Vue = V, S1 extends Store<any> = S>(options?: StoreExtendedComponentOptions<V1, S1>): StoreExtendedVue<V1, S1, {}, {}, {}, {}>;
        
    component(id: string): StoreExtendedVue<V, S>;
    component<V1 extends Vue = V, S1 extends Store<any> = S, VC extends StoreExtendedVue<V1, S1>>(id: string, constructor: VC): VC;
    component<V1 extends Vue = V, S1 extends Store<any> = S, Data, Methods, Computed, Props>(id: string, definition: StoreExtendedAsyncComponent<V1, S1, Data, Methods, Computed, Props>): StoreExtendedVue<V1, S1, Data, Methods, Computed, Props>;
    component<V1 extends Vue = V, S1 extends Store<any> = S, Data, Methods, Computed, PropNames extends string = never>(id: string, definition?: StoreExtendedThisTypedComponentOptionsWithArrayProps<V1, S1, Data, Methods, Computed, PropNames>): StoreExtendedVue<V1, S1, Data, Methods, Computed, Record<PropNames, any>>;
    component<V1 extends Vue = V, S1 extends Store<any> = S, Data, Methods, Computed, Props>(id: string, definition?: StoreExtendedThisTypedComponentOptionsWithRecordProps<V1, S1, Data, Methods, Computed, Props>): StoreExtendedVue<V1, S1, Data, Methods, Computed, Props>;
    component<V1 extends Vue = V, S1 extends Store<any> = S, PropNames extends string>(id: string, definition: StoreExtendedFunctionalComponentOptions<S1, Record<PropNames, any>, PropNames[]>): StoreExtendedVue<V1, S1, {}, {}, {}, Record<PropNames, any>>;
    component<V1 extends Vue = V, S1 extends Store<any> = S, Props>(id: string, definition: StoreExtendedFunctionalComponentOptions<S1, Props, RecordPropsDefinition<Props>>): StoreExtendedVue<V1, S1, {}, {}, {}, Props>;
    component<V1 extends Vue = V, S1 extends Store<any> = S>(id: string, definition?: StoreExtendedComponentOptions<V1, S1>): StoreExtendedVue<V1, S1, {}, {}, {}, {}>;

    use<T, V1 extends Vue = V, S1 extends Store<any> = S>(plugin: StoreExtendedPluginObject<T, V1, S1> | StoreExtendedPluginFunction<T, V1, S1>, options?: T): StoreExtendedVue<V1, S1>;
    use<V1 extends Vue = V, S1 extends Store<any> = S>(plugin: StoreExtendedPluginObject<any, V1, S1> | StoreExtendedPluginFunction<any, V1, S1>, ...options: any[]): StoreExtendedVue<V1, S1>;
    mixin<V1 extends Vue = V, S1 extends Store<any> = S>(mixin: StoreExtendedVue<V1, S1> | StoreExtendedComponentOptions<V1, S1>): StoreExtendedVue<V1, S1>;
  
    //$store: S;
};

export type StoreExtendedComponent<V extends Vue, S extends Store, Data=DefaultData<V>, Methods=DefaultMethods<V>, Computed=DefaultComputed, Props=DefaultProps> =
  StoreExtendedVue<V, S, Data, Methods, Computed, Props>
  | StoreExtendedFunctionalComponentOptions<S, Props>
  | StoreExtendedComponentOptions<never, Data, Methods, Computed, Props>;

interface EsModuleComponent {
  default: StoreExtendedComponent;
};

export interface StoreExtendedFunctionalComponentOptions<S extends Store, Props = DefaultProps, PropDefs = PropsDefinition<Props>> extends FunctionalComponentOptions<Props, PropDefs> {
    store?: S; 
};

export type StoreExtendedAsyncComponent<V extends Vue, S extends Store, Data=DefaultData<never>, Methods=DefaultMethods<never>, Computed=DefaultComputed, Props=DefaultProps>
  = StoreExtendedAsyncComponentPromise<V, S, Data, Methods, Computed, Props>
  | StoreExtendedAsyncComponentFactory<V, S, Data, Methods, Computed, Props>

export type StoreExtendedAsyncComponentPromise<V extends Vue, S extends Store, Data=DefaultData<never>, Methods=DefaultMethods<never>, Computed=DefaultComputed, Props=DefaultProps> = (
  resolve: (component: StoreExtendedComponent<V, S, Data, Methods, Computed, Props>) => void,
  reject: (reason?: any) => void
) => Promise<StoreExtendedComponent | EsModuleComponent> | void;

export type StoreExtendedAsyncComponentFactory<V extends Vue, S extends Store, Data=DefaultData<never>, Methods=DefaultMethods<never>, Computed=DefaultComputed, Props=DefaultProps> = () => {
  component: StoreExtendedAsyncComponentPromise<V, S, Data, Methods, Computed, Props>;
  loading?: StoreExtendedComponent | EsModuleComponent;
  error?: StoreExtendedComponent | EsModuleComponent;
  delay?: number;
  timeout?: number;
}

export {Vue, CombinedVueInstance};