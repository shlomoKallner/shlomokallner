

import {ImageObject} from './common';

export interface AboutUsObject {
  text: string;
  extraCss?: string;
}

export interface AboutUsPayloadObject {
    img: ImageObject;
    title?: string;
    items: AboutUsObject[];
}

export {ImageObject};