
import {LogosObject} from './logos';

export interface CsrfObject {
    csrf: string;
    nut?: string | null;
}

export interface AlertObject {
    text: string;
    title: string;
    extraCss: string;
    timeout: number;
}

export interface LaravelOptions {
    csrf: CsrfObject;
    logos: LogosObject;
    alerts: AlertObject[];
}

export type LaravelTagType = HTMLElement | string;

export interface LaravelTagsOptions {
    alerts: LaravelTagType;
    logos: LaravelTagType;
    gallery: LaravelTagType;
}

export type LaravelAnyBaseType = any | object | string | number;

export type LaravelAnyType = LaravelAnyBaseType | Array<LaravelAnyBaseType>;

export interface DebugDataObject {
  msg?: string; 
  data: Array<LaravelAnyType>;
}

export interface LaravelAdminTagsOptions extends LaravelTagsOptions {}

export interface LaravelRoutesObject extends Record<string, string> {}

export {LogosObject};