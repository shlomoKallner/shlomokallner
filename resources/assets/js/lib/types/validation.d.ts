

export interface EventHandler {
   <T extends Event> (e : T) : boolean;
}

export type DataType<T = any> = string | Array<T> | object | number | boolean;

export interface DataRetriever {
    () : object | Record<string, Array<DataType<any>> | DataType<any>>
}

export type ErrorObject = Record<string, string[]>;

export interface EventCallback {
    (errors?: ErrorObject) : boolean;
}

export interface EventHandlerGenerator {
    (data: DataRetriever, callback: EventCallback) : EventHandler;
}

export interface ValidationFunc {
    (name, value, params) : boolean;
}

export interface ValidatorExtender {
    callback: ValidationFunc;
    msg: string;
}

export type ValidatorExtentions = Record<string, ValidatorExtender>;
