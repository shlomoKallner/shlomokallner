


export interface UserInfo {
    name: string;
    email: string;
    phone: string;
}

export interface SocialInfo {
    url: string;
    type: string; /// 'fb' , 'twit' , 'g-plus' , 'link'
    //text?: string;
}

export interface ContactInfoPayload {
    user?: UserInfo;
    socialInfo?: SocialInfo[];
}
