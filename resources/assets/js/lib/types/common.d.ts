

import {AxiosResponse, AxiosError} from 'axios';

export interface SuccessCallback {
  (res: AxiosResponse<object>) : void;
};

export interface ErrorCallback {
  (err: AxiosError) : void;
};

export interface DefaultDataGetter { 
    (name: string): any; 
};

export interface MapItemGetter {
  (name: string) : string;
};

export interface ImageObject {
    img: string;
    alt?: string; 
    cap?: string;
};

export {AxiosResponse, AxiosError};