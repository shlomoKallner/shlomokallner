import { AlertObject } from './types';
export default class AlertData implements AlertObject {
    protected _text: string;
    protected _title: string;
    protected _extraCss: string;
    protected _timeout: number;
    constructor(_text?: string, _title?: string, _extraCss?: string, _timeout?: number);
    readonly extraCss: string;
    readonly title: string;
    readonly text: string;
    readonly timeout: number;
    readonly empty: boolean;
}
