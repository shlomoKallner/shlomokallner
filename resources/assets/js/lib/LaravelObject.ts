

import {
    LogosObject, CsrfObject, AlertObject, 
    GalleryPayloadObject, LaravelRoutesObject, ContactInfoPayload
} from './types';
import { StoreExtendedVue, StoreExtendedVueInstance } from './types/vue';
import { LaravelStore } from '../store';

//import Vue, {VueConstructor} from 'vue';
import Vue from 'vue';

import BootAlert from '../components/Alert.vue';
import BootLogos from '../components/Logos.vue';
import Gallery from "../components/Gallery.vue";
import Login from "../components/Login.vue";
import ContactForm from '../components/ContactForm.vue'
import ContactInfo from '../components/ContactInfo.vue'

import utils from '../utils';

export default class LaravelObject<TS extends LaravelStore = LaravelStore> {

    protected _utils: object = utils;
    protected _login: StoreExtendedVueInstance<Login, TS> | null = null;
    protected _contactForm: StoreExtendedVueInstance<ContactForm, TS> | null = null;
    protected _contactInfo: StoreExtendedVueInstance<ContactInfo, TS> | null = null;

    constructor (
        protected _vue: StoreExtendedVue<Vue, TS>,
        protected _store: TS,
        protected _logos: StoreExtendedVueInstance<BootLogos, TS>,
        protected _alerts: StoreExtendedVueInstance<BootAlert, TS>,
        protected _gallery: StoreExtendedVueInstance<Gallery, TS>,
        private _createComps: boolean = true
    ) {}

    get utils () {
        return this._utils;
    }

    get csrf () {
        return this._store.getters['Csrf/csrf'];
    }

    get nut () {
        return this._store.getters['Csrf/nut'];
    }

    get alerts () {
        return this._store.getters['Alerts/alerts'];
    }

    get login () {
        return this._login;
    }

    get debug () {
        return this._store.getters['Debug/debug'];
    }

    set login (login : StoreExtendedVueInstance<Login, TS>) {
        this._login = login;
    }

    createLogin (loginRoute: string, resetRoute: string, el : string | HTMLElement = '#login') {
        if (this._login !== null || loginRoute === '' 
            || resetRoute === '' || el === '' || el === null
        ) {
            return false;
        } else {
            this._login = new this._vue ( 
                {
                    el: el,
                    template: `<login-component :loginRoute="loginRoute" :resetRoute="resetRoute"></login-component>`,
                    store: this._store,
                    data() {
                        return {
                        loginRoute: loginRoute !== '' ? loginRoute : this.$store.getters['Urls/url']('login'),
                        resetRoute: resetRoute !== '' ? resetRoute : this.$store.getters['Urls/url']('reset')
                        }
                    },
                    watch: {
                        '$store.state.Urls._urlMap': function (val, old) {
                        if (this.$store.getters['Debug/debug']) {
                            console.log('in app.js { login->watch() } ->', [val, old, this.loginRoute, this.resetRoute]);
                        }
                        if (this.loginRoute === '' && val.has('login')) {
                            let s = val.get('login');
                            this.loginRoute = s !== '' ? s : this.loginRoute;
                        }
                        if (this.resetRoute === '' && val.has('reset')) {
                            let s = val.get('reset');
                            this.resetRoute = s !== '' ? s : this.resetRoute;
                        }
                        if (this.$store.getters['Debug/debug']) {
                            console.log('in app.js { login->watch() } ->', [val, old, this.loginRoute, this.resetRoute]);
                        }
                        }
                    },
                    components: {
                        'login-component': Login
                    }
                }
            );
        }
        return true;
    }

    get contactForm () {
        return this._contactForm;
    }

    set contactForm (contactForm : StoreExtendedVueInstance<ContactForm, TS>) {
        this._contactForm = contactForm;
    }

    createContactForm (contactRoute: string, el: string | HTMLElement = '#contactFormContainer') {
        if (this._contactForm !== null) {
            return true;
        } else {
            this._contactForm = new this._vue ( 
                {
                    el: el,
                    template: `<contact-form-component :contactRoute="contactRoute"></contact-form-component>`,
                    store: this._store,
                    data() {
                        return {
                            contactRoute: contactRoute !== '' ? contactRoute : this.$store.getters['Urls/url']('contact')
                        };
                    },
                    watch: {
                        '$store.state.Urls._urlMap': function (val, old) {
                            if (this.$store.getters['Debug/debug']) {
                                console.log('in app.js { contactRoute->watch() } ->', [val, old, this.contactRoute]);
                            }
                            if (this.loginRoute === '' && val.has('login')) {
                                let s = val.get('contact');
                                this.contactRoute = s !== '' ? s : this.contactRoute;
                            }
                            if (this.$store.getters['Debug/debug']) {
                                console.log('in app.js { contactRoute->watch() } ->', [val, old, this.contactRoute]);
                            }
                        }
                    },
                    components: {
                        'contact-form-component': ContactForm
                    }
                }
            );
        }
        return true;
    }

    get contactInfo () {
        return this._contactInfo;
    }

    set contactInfo (contactInfo : StoreExtendedVueInstance<ContactInfo, TS>) {
        this._contactInfo = contactInfo;
    }

    createContactInfo (el: string | HTMLElement = '#contactInfo') {
        if (this._contactInfo !== null) {
            return true;
        } else {
            // let ciExVue = this._vue.extend<ContactInfo>({
            //     components: {
            //         ContactInfo
            //     },
            //     template: `<contact-info-component></contact-info-component>`,
            //     store: this._store
            // });
            // this._contactInfo = new ciExVue (
            //     {
            //         el: el
            //     }
            // );
            this._contactInfo = new this._vue ( 
                {
                    el: el,
                    components: {
                        'contact-info-component': ContactInfo
                    },
                    template: `<contact-info-component></contact-info-component>`,
                    store: this._store
                }
            );
        }
        return true;
    }

    protected getData (data : string | object, _default: object = {}) {
        if (typeof data === 'string') {
            return data !== '' ? JSON.parse(data) : _default;
        } 
        return data;
    }

    resetData (
        routes : string | LaravelRoutesObject, 
        csrf : string | CsrfObject, 
        logos : string | LogosObject, 
        alerts : string | AlertObject[], 
        gallery : string | GalleryPayloadObject, 
        contactInfo: string | ContactInfoPayload,
        scrollPast : number = 50
    ) {
        let _routes = this.getData(routes);
        this._store.resetStore(
            _routes, this.getData(csrf), 
            this.getData(logos), this.getData(alerts), 
            this.getData(gallery), this.getData(contactInfo), 
            scrollPast
        );
        if (this._createComps) {
            if (Object.prototype.hasOwnProperty.call(_routes, 'login') 
                && Object.prototype.hasOwnProperty.call(_routes, 'reset')
            ) {
                this.createLogin (_routes['login'], _routes['reset'], '#login');
            }
            if (Object.prototype.hasOwnProperty.call(_routes, 'contact')) {
                this.createContactForm (_routes['contact'], '#contactFormContainer');
            }
        }
    }
}