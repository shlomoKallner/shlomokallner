

import {CsrfObject} from './types'
import _ from 'lodash-es'

export function toCsrf (csrf: string | CsrfObject) {
    //return csrf instanceof string || typeof csrf === 'string' ? {csrf} : csrf ;
    return typeof csrf === 'string' ? {csrf} : csrf ;
}

export function isValid(data) : boolean {
    return data !== undefined && data !== null;
}

export function title_case(str: string) : string {
    let tmp = str.split(/\s/);
    let res = [];
    if (tmp.length > 0) {
        for (let index = 0; index < tmp.length; index++) {
            res.push(_.capitalize(tmp[index]));
        }
    }
    return res.length > 0 ? res.join(' ') : '';
}

export function get_uiv_Notification_type(str: string) : string {
    let t1 = str.trim();
    let t2 = t1.split(/\s/);
    let filter = ['info' , 'success' , 'warning' , 'danger'];
    let res = [];
    if (t2.length > 0) {
        for (let index = 0; index < t2.length; index++) {
            const txt = t2[index];
            if (txt in filter) {
                res.push(txt);
            }
        }
    }
    return res.length > 0 ? res[0] : '';
}

export function get_uiv_Notification_custom_classes(str: string) : string {
    let t1 = str.trim();
    let t2 = t1.split(/\s/);
    let filter = ['info' , 'success' , 'warning' , 'danger'];
    let res = [];
    if (t2.length > 0) {
        for (let index = 0; index < t2.length; index++) {
            const txt = t2[index];
            if (!(txt in filter)) {
                res.push(txt);
            }
        }
    }
    return res.length > 0 ? res.join(' ') : '';
}

export interface PropertySetter {
    (val:any): void;
}

export interface PropertyGetter {
    ():any;
}

export function set_Prop_on_Object_if_undefined(
    obj: object, 
    prop: string | symbol | number, 
    value: any, 
    writable: boolean = true,
    enumerable: boolean = true,
    configurable: boolean = true
    //get?: PropertyGetter,
    //set?: PropertySetter

) {
    let tds = Object.getOwnPropertyDescriptor(obj, prop);
    if (tds === undefined && (!Object.isFrozen(obj) && !Object.isSealed(obj))) {
        Object.defineProperty(
            obj, prop, {
                value,
                // set,
                // get,
                writable,
                enumerable,
                configurable
            }
        );
    }
}