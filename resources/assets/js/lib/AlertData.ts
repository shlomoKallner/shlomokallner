
import {AlertObject} from './types'
import _ from 'lodash-es'

export default class AlertData implements AlertObject {
    constructor(protected _text:string = '', protected _title:string = 'Alert', protected _extraCss:string = 'info', protected _timeout:number = 9000) {}
    get extraCss(): string {
      return this._extraCss
    }
    get title(): string {
      return this._title
    }
    get text(): string {
      return this._text
    }
    get timeout(): number {
        return this._timeout
    }

    get empty(): boolean {
      return _.isEmpty(this._text) || _.isEmpty(this._title) || _.isEmpty(this._extraCss)
    }
}