
//require('@babel/polyfill')
import "@babel/polyfill";
//import _ from 'lodash'; // removed for reasons of code duplication removal 
                          //(components usin the '*-es' version..).
import _ from 'lodash-es';
import './lib/jquery-leaked';

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

// try {
    
        
//     }

//     //require('bootstrap-sass');
// } catch (e) {}
import 'bootstrap-sass';
import '../sass/font-awesome-4.7.0/css/font-awesome.css';
import 'vue-tel-input/dist/vue-tel-input.css';
import axios from 'axios';
import Vue from 'vue';
import vuex from 'vuex';
//import VueAxios from 'vue-axios';
import * as uiv from 'uiv';
window._ = _; //require('lodash'); 

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = axios; // require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: 'your-pusher-key',
//     cluster: 'mt1',
//     encrypted: true
// });

window.Vue = Vue;
window.Vue.use(vuex);
window.Vue.use(uiv, {prefix: 'uiv'});


/// Some common components:

import BootAlert from './components/Alert.vue';
import BootLogos from './components/Logos.vue';
import VueTelInput from 'vue-tel-input';
window.Vue.component('alert-component', BootAlert);
window.Vue.component('logos-component', BootLogos);
window.Vue.component('field-tel-input', VueTelInput);
