import Vue from 'vue';
export default class BootLogos extends Vue {
    readonly url: any;
    readonly useStick: any;
    readonly img: any;
    readonly alt: any;
    readonly stick: any;
    readonly main: any;
}
