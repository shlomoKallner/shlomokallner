import UtilityMethods from './Utility';
import { AjaxResponse, AjaxError } from './Utility';
declare const Login_base: import("vue-class-component/lib/declarations").VueClass<UtilityMethods>;
export default class Login extends Login_base {
    showLogin: boolean;
    showReset: boolean;
    toggleCollapse(): void;
    showModal: boolean;
    beforeClose(msg?: string): boolean;
    openModal(): boolean;
    beforeMount(): void;
    beforeUpdate(): void;
    setRoutes(): void;
    pfSetRouteMap(val: any, old: any): void;
    loginRoute: string;
    p_loginRoute: string;
    readonly pfLoginRoute: string;
    resetRoute: string;
    p_resetRoute: string;
    readonly pfResetRoute: string;
    loginForm: {
        data: {
            email: string;
            password: string;
            remember: boolean;
        };
        styles: {
            email: string[];
            password: string[];
        };
        errors: {
            email: any[];
            password: any[];
        };
    };
    getLoginData(): {
        email: string;
        password: string;
        remember: boolean;
    };
    loginCallback(err?: any): boolean;
    validateLogin<T extends Event>(e: T): boolean;
    resetForm: {
        data: {
            email: string;
        };
        styles: {
            email: string[];
        };
        errors: {
            email: any[];
        };
        response: {
            styles: string[];
            content: string;
        };
    };
    resetOnSuccessCB(res: AjaxResponse): void;
    resetOnFailureCB(err: AjaxError): void;
    resetCallback(err?: any): boolean;
    validateReset<T extends Event>(e: T): boolean;
}
export {};
