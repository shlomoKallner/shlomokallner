import UtilityMethods from './Utility';
declare const FormResponse_base: import("vue-class-component/lib/declarations").VueClass<UtilityMethods>;
export default class FormResponse extends FormResponse_base {
    styles: string[];
    content: string;
}
export {};
