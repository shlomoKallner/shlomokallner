import Vue from 'vue';
import { GalleryObject } from '../lib/types';
export default class Gallery extends Vue {
    useMixItUp: boolean;
    readonly title: string;
    readonly sortings: string[];
    readonly items: GalleryObject[];
    selectedFilter: string;
    protected filterFunc(val: GalleryObject, key?: string | number, collection?: GalleryObject[]): boolean;
    readonly filteredItems: GalleryObject[];
    empty(data: any): boolean;
    toTitle(str: string): string;
    toFilter(str: string): string;
}
