
import {Vue, Component, Watch} from "vue-property-decorator";
import _ from 'lodash-es';
import Vuex from 'vuex';
import {AxiosResponse, AxiosError} from 'axios';
import {
  SuccessCallback, ErrorCallback, DataRetriever, 
  EventCallback, DefaultDataGetter, MapItemGetter,
  DebugDataObject
} from '../lib/types';

Vue.use(Vuex);

export type AjaxResponse = AxiosResponse<object>;

export {AxiosError as AjaxError, MapItemGetter};

@Component({})
export default class UtilityMethods extends Vue {

  empty(data: any) {
    return _.isEmpty(data);
  }

  resetDataAux(formField: string, getterCB: DefaultDataGetter) {
    if (Object.prototype.hasOwnProperty.call(this, formField)) {
      for (let i in this[formField].data) {
        this[formField].data[i] = getterCB(i);
      }
    }
  }

  resetFormAux(formField: string) {
    if (Object.prototype.hasOwnProperty.call(this, formField)) {
      if (Object.prototype.hasOwnProperty.call(this[formField], 'styles')) {
        for (let i in this[formField].styles) {
          this[formField].styles[i] = ['form-group'];
        }
      }
      if (Object.prototype.hasOwnProperty.call(this[formField], 'errors')) {
        for (let i in this[formField].errors) {
          this[formField].errors[i] = [];
        }
      }
    }
  }

  setFormValidationErrors(formField: string, err?: Record<string, string[]>, extraCss?: string[]) {
    if (err && Object.prototype.hasOwnProperty.call(this, formField))  {
      if (Object.prototype.hasOwnProperty.call(this[formField], 'errors')) {
        for (let i in err) {
          if (Object.prototype.hasOwnProperty.call(this[formField].errors, i)) {
            this[formField].errors[i] = err[i];
          }
          if (extraCss && Object.prototype.hasOwnProperty.call(this[formField].styles, i)) {
            this[formField].styles[i] = extraCss;
          }
          //this[formField].errors[i] = [];
        }
      }
    }
  }

  setFormErrorsAux(formField: string, error: AxiosError) {
    if (Object.prototype.hasOwnProperty.call(this, formField)) {
      if (Object.prototype.hasOwnProperty.call(this[formField], 'response')) {
        this[formField].response.styles = ['alert', 'alert-danger'];
        if (error.response) {
          this[formField].response.content = error.response.data.message;
        } else if (error.request) {
          this[formField].response.content = 'Whoops! Our request has timedout!';
        } else {
          this[formField].response.content = 'Arggh! Unable to send request! ' + error.message;
        }
      }
    }
  }

  setFormSuccessAux(formField: string, res: AjaxResponse) {
    if (Object.prototype.hasOwnProperty.call(this, formField)) {
      if (Object.prototype.hasOwnProperty.call(this[formField], 'response')) {
        if (Object.prototype.hasOwnProperty.call(res.data, 'message')) {
          this[formField].response.styles = ['alert', 'alert-info'];
          this[formField].response.content = res.data['message'];
        }
      }
    }
  }

  // (data: DataRetriever, callback: EventCallback) : EventHandler
  validatorGetter<T extends Event>(
    e : T, validator: string, formField: string, 
    getValidationData: DataRetriever,
    validationCallback: EventCallback
  ) : boolean {
    let dbg = this.doGetDebugger('validatorGetter<> () : boolean');
    dbg(
      {
        msg: 'in validatorGetter() ->', 
        data: [
          {
            obj: this, 
            event: e, 
            validator: validator, 
            formField: formField,
            routesMap: this.p$_routesMap
          }
        ]
      }
    );
    // if (this.p$_debug) {
    //   console.log(
    //     'in validatorGetter() ->', 
    //     {
    //       obj:this, event:e, validator:validator, formField:formField,
    //       routesMap: this.p$_routesMap
    //     }
    //   );
    // }
    let v1 = this.$store.getters['Validation/' + validator];
    let v2 = v1( 
      getValidationData, 
      validationCallback
    );
    this.resetFormAux(formField);
    return v2(e);
  }

  p$_routesMap : Map<string, string> | null = null; 

  @Watch('$store.state.Urls._urlMap')
  pf$_watchRouteMap (val, old) {
    for (const iter of Array.from<string>(this.p$_routesMap.keys())) {
      if (val.has(iter)) {
        let s = val.get(iter);
        let o = this.p$_routesMap.get(iter);
        if (s !== '' && (o === '' || o !== s)) {
          this.p$_routesMap.set(iter, s);
        } 
      }
    }
    _.noop(old);
  }

  private checkRouter() {
    if (this.p$_routesMap === null || this.p$_routesMap === undefined) {
      this.p$_routesMap = new Map<string, string>();
    }
  }

  private checkRoute(name:string) : boolean {
    //this.checkRouter();
    // if (!this._routesMap.has(name) || this._routesMap.get(name) === '') {
    //   this.ourRouter = name;
    // }
    return this.p$_routesMap.has(name) || this.p$_routesMap.get(name) !== '';
  }

  set ourRoutes (name : string[]) {
    this.checkRouter();
    let arr :string[] = [];
    if (_.isString(name) && name !== '') {
      arr.push(name);
    } else if (_.isArray(name)) {
      arr = name;
    }
    for (let index = 0; index < arr.length; index++) {
      const element = arr[index];
      let p_default : string = '';
      let path : string = this.$store.getters['Urls/url'](element);
      if (!this.checkRoute(element)) {
        this.p$_routesMap.set(element, path !== p_default ? path : p_default);
      }
    }
  }

  get ourRoutes () : string[] {
    this.checkRouter();
    let dbg = this.doGetDebugger('get ourRoutes () : string[]');
    let r : string[] = Array.from<string>(this.p$_routesMap.keys());
    // let t : {key : string, value : string};
    // for (let {key, value} of this._routesMap) {
    //   r.push(key);
    //   _.noop(value);
    // }
    let map = this.p$_routesMap;
    dbg({msg:'hello! ->', data:[r, map]});
    // if (this.p$_debug) {
    //   console.log('hello! ->', {r, map:this.p$_routesMap});
    // }
    return r;
  }

  ourRouter (name : string) : string {
    this.checkRouter();
    if (!this.checkRoute(name)) {
      this.ourRoutes = [name];
    }
    return this.p$_routesMap.get(name);
  }

  // beforeCreate() {
  //   this.setRoutes();
  // }

  // setRoutes () : void {
  //   _.noop();
  // }

  get p$_ld () {
    // return _ || this.$store.getters['lodash'];
    return this.$store.getters['lodash'];
  }

  get p$_axios () {
    return this.$store.getters['axios'];
  }

  get p$_csrf () {
    return this.$store.getters['Csrf/csrf'];
  }

  get p$_nut () {
    return this.$store.getters['Csrf/nut'];
  }

  get p$_debug () {
    return this.$store.getters['Debug/debug'];
  }

  doAjaxPost(
    path: string, data: object, onSuccess: SuccessCallback,
    onFailure: ErrorCallback, timeout: number = 3000
  ) : void {
    //let a = this.$store.getters['axios'];
    this.p$_axios(
      {
        method: 'POST',
        url: path,
        data: {
          _token: this.p$_csrf,
          token: this.p$_csrf,
          nut: this.p$_nut,
          _nut: this.p$_nut,
          data: data
        },
        withCredentials: true,
        timeout: timeout
      }
    ).then(onSuccess).catch(onFailure);
  }

  doGetDebugger(funcName: string) {
    let pl_self = this;
    return function (data: DebugDataObject) : boolean {
      if (pl_self.p$_debug) {
        let str = data.msg ? ' { ' + data.msg + ' } -> ' : ' -> ';
        console.log(funcName + str, ...data.data);
      }
      return true;
    }
  }

}

