import { Vue } from 'vue-property-decorator';
import { AlertObject } from '../lib/types';
export default class BootAlert extends Vue {
    useAlerts: boolean;
    _alerts: AlertObject[];
    readonly alerts: any;
    readonly usingAlerts: boolean;
    empty(data: any): boolean;
    emptyAlert(alert: AlertObject): boolean;
    remAlert(alert: AlertObject): void;
    extraText(text: String): any[];
    onAlertsChanged(val: AlertObject[], oldVal: AlertObject[]): void;
}
