import UtilityMethods from './Utility';
import { AjaxResponse, AjaxError } from './Utility';
declare const ContactForm_base: import("vue-class-component/lib/declarations").VueClass<UtilityMethods>;
export default class ContactForm extends ContactForm_base {
    readonly className: string;
    beforeMount(): void;
    beforeUpdate(): void;
    setRoutes(): void;
    contactRoute: string;
    p_contactRoute: string;
    readonly pfContactRoute: string;
    pfSetRouteMap(val: any, old: any): void;
    isPhoneValid: boolean;
    thisForm: {
        data: {
            name: string;
            email: string;
            phone: string;
            message: string;
        };
        errors: {
            name: any[];
            email: any[];
            phone: any[];
            message: any[];
        };
        styles: {
            name: string[];
            email: string[];
            phone: string[];
            message: string[];
        };
        response: {
            styles: any[];
            content: string;
        };
    };
    validatePhone({ number, isValid, country }: {
        number: any;
        isValid: any;
        country: any;
    }): void;
    getValidationData(): {
        name: string;
        email: string;
        phone: string;
        message: string;
    };
    onSuccessCB(res: AjaxResponse): void;
    onFailureCB(err: AjaxError): void;
    validationCallback(err?: any): boolean;
    validateForm<T extends Event>(e: T): boolean;
}
export {};
