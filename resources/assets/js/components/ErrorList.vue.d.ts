import UtilityMethods from './Utility';
declare const ErrorList_base: import("vue-class-component/lib/declarations").VueClass<UtilityMethods>;
export default class ErrorList extends ErrorList_base {
    errors: string[];
    type: string;
}
export {};
