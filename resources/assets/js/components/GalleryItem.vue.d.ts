import Vue from 'vue';
import { ImageObject } from '../lib/types';
export default class GalleryItem extends Vue {
    img: ImageObject;
    url: string;
    title: string;
    summary: string;
    content: string;
    sortings: string[];
    labelCss: string;
    modalOpen: boolean;
    useBtnModalTrigger: boolean;
    usePTagsInModalBody: boolean;
    usePTagsInModalTrigger: boolean;
    closeModal(msg?: string): void;
    arrayToLower(strArr: string[]): string[];
}
