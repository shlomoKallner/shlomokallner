import UtilityMethods from './Utility';
import { SocialInfo, UserInfo } from '../lib/types';
declare const ContactInfo_base: import("vue-class-component/lib/declarations").VueClass<UtilityMethods>;
export default class ContactInfo extends ContactInfo_base {
    title: string;
    readonly userInfo: UserInfo;
    readonly socialInfo: SocialInfo[];
}
export {};
