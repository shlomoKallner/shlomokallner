import { Vue } from "vue-property-decorator";
import { AxiosResponse, AxiosError } from 'axios';
import { SuccessCallback, ErrorCallback, DataRetriever, EventCallback, DefaultDataGetter, MapItemGetter, DebugDataObject } from '../lib/types';
export declare type AjaxResponse = AxiosResponse<object>;
export { AxiosError as AjaxError, MapItemGetter };
export default class UtilityMethods extends Vue {
    empty(data: any): boolean;
    resetDataAux(formField: string, getterCB: DefaultDataGetter): void;
    resetFormAux(formField: string): void;
    setFormValidationErrors(formField: string, err?: Record<string, string[]>, extraCss?: string[]): void;
    setFormErrorsAux(formField: string, error: AxiosError): void;
    setFormSuccessAux(formField: string, res: AjaxResponse): void;
    validatorGetter<T extends Event>(e: T, validator: string, formField: string, getValidationData: DataRetriever, validationCallback: EventCallback): boolean;
    p$_routesMap: Map<string, string> | null;
    pf$_watchRouteMap(val: any, old: any): void;
    private checkRouter;
    private checkRoute;
    ourRoutes: string[];
    ourRouter(name: string): string;
    readonly p$_ld: any;
    readonly p$_axios: any;
    readonly p$_csrf: any;
    readonly p$_nut: any;
    readonly p$_debug: any;
    doAjaxPost(path: string, data: object, onSuccess: SuccessCallback, onFailure: ErrorCallback, timeout?: number): void;
    doGetDebugger(funcName: string): (data: DebugDataObject) => boolean;
}
