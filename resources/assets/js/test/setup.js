const jsdom_global = require('jsdom-global');
const chai = require('chai');
const sinon = require('sinon');

global.assert = chai.assert;
global.expect = chai.expect;
global.sinon = sinon;
jsdom_global();
