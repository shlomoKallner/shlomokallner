

import {isValid} from './lib/utils'

export default {
  doFormAjax: function (e, formSel, resSel, subSel, 
      doSubmit = false, usingHelpSpan = false, 
      dataType = 'json', debug = false, $ = window.jQuery) {

      e.preventDefault();

      var formEl          = $(formSel),
          postData 		= formEl.serializeArray(),
          formURL 		= formEl.attr("action"),
          formMethod      = formEl.attr("method"),
          $cfResponse 	= formEl.find('div' + resSel),
          $cfsubmit 		= formEl.find('button[type=submit]' + subSel),
          cfsubmitText 	= $cfsubmit.text(),
          token           = window.Laravel.csrf;

      $cfResponse.empty();
      $cfsubmit.text("Sending...");

      if (debug) {
          console.log('in app.js');
      }

      $.ajax(
          {
              url : formURL,
              type: formMethod,
              data : postData,
              dataType: dataType,
              headers: {
                'X-CSRF-TOKEN': token,
                'X-XSRF-TOKEN': token
              },
              xhrFields: {
                withCredentials: true
              },
              success:function(data, status, xhr)
              {
                  if (debug) {
                      console.log(status, xhr);
                  }
                  $.each(postData, function (i, field) {
                      var sel = usingHelpSpan 
                          ? '[id*="' + field.name + '-error"]'
                          : ' #' + field.name;
                      var c = formEl.find(sel);
                      var par = c.parent('.form-group');
                      if (usingHelpSpan) {
                          c.empty();
                      }
                      if (data.response === 'okay') {
                          par.removeClass('has-error');
                      } else {
                          if (field.name in data.errors.list) {
                              par.addClass('has-error');
                              if (usingHelpSpan) {
                                  c.html(data.errors.data[field.name]);
                              }
                          } else {
                              par.removeClass('has-error');
                          }
                      }
                  });
                  $cfsubmit.text(cfsubmitText);
                  if (data.response === 'okay') {
                      $cfResponse.html(data.html);
                      if (data.refresh === 'refresh') {
                          window.location.reload()
                      }
                  } else if (data.response === 'error') {
                      $cfResponse.empty();
                  }
              },
              error: function (xhr, status, error)
              {
                  var str = 'Error occurred!';
                  $cfResponse.html('<p class="text-danger">' + str + ' Please try again</p>');
                  //alert("Error occurred! Please try again");
                  if (debug) {
                      console.log(str + ' : ' + error, status, xhr);
                  }
              }
          }
      );

      return doSubmit;
  },
  getData: function (data) {
      if (typeof data === 'string') {
          return JSON.parse(data);
      } else /* if (typeof data === 'object') */ {
          return data;
      }
      // return null;
  },
  isDef: isValid
}