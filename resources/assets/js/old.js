

export default {
  logos: {
    main: {},
    stick: {},
    isset: false
  },
  setLogos: function (main, stick, debug = false) {
      if (debug) {
        console.log('this.logos.main => ' + this.logos.main)
        console.log('main => ' + main)
        console.log('this.logos.stick => ' + this.logos.stick)
        console.log('stick => ' + stick)
      }
      this.logos.main = Object.assign(this.logos.main, this.utils.getData(main))
      this.logos.stick = Object.assign(this.logos.stick, this.utils.getData(stick))
      if (!this.logos.isset) {
          window.jQuery(function(){
              window.jQuery(window).on( "scroll", function() {
                  if (window.jQuery(document).scrollTop() > 50) {
                      window.jQuery("#logo").attr("src", window.Laravel.logos.stick.img)
                  }
                  else {
                      window.jQuery("#logo").attr("src", window.Laravel.logos.main.img)
                  }
              });
          });
          this.logos.isset = true;
      }
      if (debug) {
        console.log('this.logos.main => ' + this.logos.main)
        console.log('main => ' + main)
        console.log('this.logos.stick => ' + this.logos.stick)
        console.log('stick => ' + stick)
      }
      
  }
}