import { Store } from 'vuex';
import { AxiosInstance } from 'axios';
import { LogosObject, CsrfObject, AlertObject, GalleryPayloadObject, LaravelRoutesObject, ContactInfoPayload } from '../lib/types';
export default class LaravelStore extends Store<any> {
    protected _axios: AxiosInstance;
    constructor(_axios: AxiosInstance, debug?: boolean, baseUrl?: string, csrf?: CsrfObject, logos?: LogosObject, alerts?: AlertObject[], gallery?: GalleryPayloadObject, contactInfo?: ContactInfoPayload, scrollPast?: number);
    resetStore(routes: LaravelRoutesObject, csrf: CsrfObject, logos: LogosObject, alerts: AlertObject[], gallery: GalleryPayloadObject, contactInfo: ContactInfoPayload, scrollPast: number): void;
    runUpdates(): Promise<void>;
}
