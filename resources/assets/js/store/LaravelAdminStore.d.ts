import LaravelStore from './LaravelStore';
import { AxiosStatic } from 'axios';
import { LogosObject, CsrfObject, AlertObject, GalleryPayloadObject, ContactInfoPayload } from '../lib/types';
export default class LaravelAdminStore extends LaravelStore {
    constructor(axios: AxiosStatic, debug?: boolean, baseUrl?: string, csrf?: CsrfObject, logos?: LogosObject, alerts?: AlertObject[], gallery?: GalleryPayloadObject, contactInfo?: ContactInfoPayload, scrollPast?: number);
}
