
import Vue from 'vue';
import { Module, VuexModule, Mutation } from 'vuex-module-decorators';
import {ImageObject, LogosPayloadObject} from '../../lib/types';
// , LogosObject

@Module({name: 'logos', namespaced: true})
export default class Logos extends VuexModule {
    _main : ImageObject = {img: '', alt: '', cap: ''};

    get main () : ImageObject {
        return this._main;
    }

    @Mutation
    setMain (newMain: ImageObject) {
        Vue.set(this, '_main', newMain);
        //this._main = newMain;
    }

    _stick : ImageObject = {img: '', alt: '', cap: ''};

    get stick () : ImageObject {
        return this._stick;
    }

    @Mutation
    setStick (newStick: ImageObject) {
        Vue.set(this, '_stick', newStick);
        //this._stick = newStick;
    }

    _useStick : boolean = false;

    get useStick () : boolean {
        return this._useStick;
    }

    @Mutation
    setUseStick (newUS: boolean) {
        this._useStick = newUS;
    }

    @Mutation
    setAll (logos: LogosPayloadObject) {
        Vue.set(this, '_main', logos.logos.main);
        //this._main = logos.logos.main;
        Vue.set(this, '_stick', logos.logos.stick);
        //this._stick = logos.logos.stick;
        this._scrollPast = logos.scrollPast;
    }

    _scrollPast : number = 50;

    get scrollPast () : number {
        return this._scrollPast;
    }

    @Mutation
    setScrollPast(scrollPast: number) {
        this._scrollPast = scrollPast;
    }
}