
//import Vue from 'vue';
import {Module, VuexModule, Mutation} from 'vuex-module-decorators';
import {UserInfo, SocialInfo, ContactInfoPayload} from '../../lib/types';
import _ from 'lodash-es';
import {addItemToArray, setItemOnObject} from './utils';

@Module({name: 'contactInfo', namespaced: true})
export default class ContactInfo extends VuexModule {
    _user : UserInfo = {
        name: '',
        phone: '',
        email: ''
    };

    get user () {
        return this._user;
    }

    _socialInfo : SocialInfo[] = [];

    get socialInfo () {
        return this._socialInfo;
    }

    @Mutation
    resetAll(payload: ContactInfoPayload) {
        // if (payload.user) {
        //     setItemOnObject<UserInfo, typeof this>(this, '_user', payload.user);
        //     Vue.set(this._user, 'name', payload.user.name);
        //     Vue.set(this._user, 'phone', payload.user.phone);
        //     Vue.set(this._user, 'email', payload.user.email);
        // }
        setItemOnObject<UserInfo, ContactInfo>(this, '_user', payload.user);
        addItemToArray<SocialInfo, ContactInfo>(this, '_socialInfo', payload.socialInfo);
        // if (payload.socialInfo && !_.isEmpty(payload.socialInfo)) {
        //     let fs = _.differenceWith(payload.socialInfo, this._socialInfo, _.isEqual);
        //     if (!_.isEmpty(fs)) {
        //         let sorts = _.concat(this._socialInfo, fs);
        //         this._socialInfo = [];
        //         for (let i = 0; i < sorts.length; i++) {
        //             Vue.set(this._socialInfo, i, sorts[i]);
        //         }
        //     }
        // }
    }
}