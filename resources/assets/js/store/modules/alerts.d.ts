import { VuexModule } from 'vuex-module-decorators';
import { AlertObject } from '../../lib/types';
export default class Alerts extends VuexModule {
    _alerts: AlertObject[];
    readonly alerts: AlertObject[];
    setAlerts(alerts?: AlertObject[]): void;
    remAlert(alerts?: AlertObject[]): void;
    addAlerts(alerts?: AlertObject[]): void;
}
