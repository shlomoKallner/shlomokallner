import { VuexModule } from 'vuex-module-decorators';
import { GalleryObject, GalleryPayloadObject } from '../../lib/types';
export default class Gallery extends VuexModule {
    _items: GalleryObject[];
    readonly items: GalleryObject[];
    _sortings: string[];
    readonly sortings: string[];
    _title: string;
    readonly title: string;
    setAll(payload?: GalleryPayloadObject): void;
    sub(payload: GalleryPayloadObject): void;
    add(payload: GalleryPayloadObject): void;
}
