import { VuexModule } from 'vuex-module-decorators';
export default class Debug extends VuexModule {
    _debug: boolean;
    readonly debug: boolean;
    setAll(debug: boolean): void;
}
