import { VuexModule } from 'vuex-module-decorators';
import { CsrfObject } from '../../lib/types';
export default class Csrf extends VuexModule {
    _csrf: string;
    _nut: string;
    readonly csrf: string;
    setCsrf(newCsrf: string): void;
    setCsrfAsync(newCsrf: string): string;
    readonly nut: string;
    setNut(newNut: string): void;
    setNutAsync(newNut: string): string;
    setAll(newCsrf: CsrfObject): void;
}
