import { VuexModule } from 'vuex-module-decorators';
import { UserInfo, SocialInfo, ContactInfoPayload } from '../../lib/types';
export default class ContactInfo extends VuexModule {
    _user: UserInfo;
    readonly user: UserInfo;
    _socialInfo: SocialInfo[];
    readonly socialInfo: SocialInfo[];
    resetAll(payload: ContactInfoPayload): void;
}
