import { VuexModule } from 'vuex-module-decorators';
import { EventHandler, DataType, DataRetriever, EventCallback, EventHandlerGenerator, ValidationFunc, ValidatorExtender, ValidatorExtentions } from '../../types';
export { DataType, ValidationFunc, ValidatorExtender };
export declare function validatorFn(data: DataRetriever, callback: EventCallback, rules: object, msgs?: object, names?: object, extentions?: ValidatorExtentions): EventHandler;
export declare function validatorGen(rules: object, msgs?: object, names?: object, extentions?: ValidatorExtentions): EventHandlerGenerator;
export default class Validation extends VuexModule {
    _email: {
        rules: {
            email: string;
        };
        msgs: {
            'email.required': string;
        };
        names: {};
        extentions: {};
    };
    _password: {
        rules: {
            password: string;
        };
        msgs: {
            'password.required': string;
        };
        names: {};
        extentions: {
            'mypasswordregexp': {
                msg: string;
                callback: (name: any, value: any, params: any) => boolean;
            };
        };
    };
    private static _text_gen_rules;
    _contactForm: {
        rules: {
            name: string;
            phone: string;
            message: string;
        };
        msgs: {};
        names: {};
        extentions: {};
    };
    readonly contactForm: EventHandlerGenerator;
    readonly login: EventHandlerGenerator;
    readonly passwordReset: EventHandlerGenerator;
}
