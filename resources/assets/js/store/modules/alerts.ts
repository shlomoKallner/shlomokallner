import Vue from 'vue';
import { Module, VuexModule, Mutation} from 'vuex-module-decorators';
import {AlertObject} from '../../lib/types';
import _ from 'lodash-es';

@Module({name: 'alerts', namespaced: true})
export default class Alerts extends VuexModule {
    _alerts: AlertObject[] = [];

    get alerts () : AlertObject[] {
        return this._alerts;
    }

    @Mutation
    setAlerts (alerts?: AlertObject[]) {

        //Vue.set(this, '_alerts', newAlerts);
        //this._alerts = newAlerts;
        //this.reset(alerts);
        if (alerts && !_.isEmpty(alerts)) {
            this._alerts = [];
            for (let index = 0; index < alerts.length; index++) {
                const element = alerts[index];
                Vue.set(this._alerts, index, element);
            }
        }
    }

    @Mutation
    remAlert (alerts?: AlertObject[]) {
        //let a =_.pull(this._alerts, alerts);
        //this.reset(a);
        //this.mutations[]
        //return a;
        if (alerts && !_.isEmpty(alerts)) {
            for (let i = 0; i < alerts.length; i++) {
                const elem = alerts[i];
                let idx = _.findIndex(this._alerts, obj => _.isEqual(elem, obj));
                if (idx >= 0) {
                    Vue.delete(this._alerts, idx);
                }
            }
        }
    }

    @Mutation
    addAlerts (alerts?: AlertObject[]) {
        //this._alerts.push(...newAlerts); 
        /// need to use _.differenceWith to create concat-ed array to reset with!
        if (alerts && !_.isEmpty(alerts)) {
            let a : AlertObject[] = _.differenceWith<AlertObject, AlertObject>(alerts, this._alerts, _.isEqual);
            //this.reset(_.concat(this._alerts , a));
            // return _.concat(this._alerts , a);
            let b = _.concat(this._alerts , a);
            this._alerts = [];
            for (let i = 0; i < b.length; i++) {
                Vue.set(this._alerts, i, b[i]);
            }
        }
    }
}