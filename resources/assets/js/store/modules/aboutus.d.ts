import { VuexModule } from 'vuex-module-decorators';
import { AboutUsObject, AboutUsPayloadObject, ImageObject } from '../../types';
export default class AboutUs extends VuexModule {
    _items: AboutUsObject[];
    _img: ImageObject;
    _title: string;
    readonly items: AboutUsObject[];
    readonly image: ImageObject;
    readonly title: string;
    setAll(payload?: AboutUsPayloadObject): void;
    setImage(img: ImageObject): void;
    addItem(items?: AboutUsObject[]): void;
    remItem(items?: AboutUsObject[]): void;
}
