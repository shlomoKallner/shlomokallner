import { VuexModule } from 'vuex-module-decorators';
import { LaravelRoutesObject } from '../../lib/types';
export declare function checkUrl(str: string): boolean;
export default class Urls extends VuexModule {
    _baseUrl: string;
    _urlMap: Map<string, string>;
    readonly baseUrl: string;
    resetBaseUrl(baseUrl: string): void;
    readonly url: (name?: string, isDir?: boolean) => any;
    addUrl(name: string, url: string): void;
    setRoutes(routes: LaravelRoutesObject): void;
}
