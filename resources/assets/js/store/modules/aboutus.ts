import Vue from 'vue';
import { Module, VuexModule, Mutation} from 'vuex-module-decorators';
import {AboutUsObject, AboutUsPayloadObject, ImageObject} from '../../types';
import _ from 'lodash-es';

@Module({name: 'aboutus', namespaced: true})
export default class AboutUs extends VuexModule {
    _items : AboutUsObject[] = [];
    _img : ImageObject = {img:'', alt: '', cap: ''};
    _title : string = 'About us';

    get items () {
        return this._items;
    }

    get image () {
        return this._img;
    }

    get title () {
        return this._title;
    }

    @Mutation
    setAll (payload?: AboutUsPayloadObject) {
        if (payload) {
            for (const key in payload.img) {
                const element = payload.img[key];
                Vue.set(this._img, key, element);
            }
            if (payload.title) {
                this._title = payload.title;
            }
            for (let index = 0; index < payload.items.length; index++) {
                const element = payload.items[index];
                Vue.set(this._items, index, element);
            }
        }
    }

    @Mutation
    setImage (img: ImageObject) {
        for (const key in img) {
            const element = img[key];
            Vue.set(this._img, key, element);
        }
    }

    @Mutation
    addItem (items?: AboutUsObject[]) {
        if (items && !_.isEmpty(items)) {
            let a : AboutUsObject[] = _.differenceWith<AboutUsObject, AboutUsObject>(items, this._items, _.isEqual);
            let b = _.concat(this._items , a);
            this._items = [];
            for (let i = 0; i < b.length; i++) {
                Vue.set(this._items, i, b[i]);
            }
        }
    }

    @Mutation
    remItem (items?: AboutUsObject[]) {
        if (items && !_.isEmpty(items)) {
            for (let i = 0; i < items.length; i++) {
                const elem = items[i];
                let idx = _.findIndex(this._items, obj => _.isEqual(elem, obj));
                if (idx >= 0) {
                    Vue.delete(this._items, idx);
                }
            }
        }
    }

}