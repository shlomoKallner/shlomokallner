
import _ from 'lodash-es';
import Vue from 'vue';

export function remItemFromArray<T>(array: T[], items?:T[]) {
    if (items && !_.isEmpty(items)) {
        for (let i = 0; i < items.length; i++) {
            const elem = items[i];
            let idx = _.findIndex(array, obj => _.isEqual(elem, obj));
            if (idx >= 0) {
                Vue.delete(array, idx);
            }
        }
    }
    return array;
}

export function addItemToArray<T, O extends Object>(obj: O, field: string, items?:T[]) {
    if (Object.prototype.hasOwnProperty.call(obj, field) && _.isArray(obj[field])) {
        if (items && !_.isEmpty(items)) {
            let a : T[] = _.differenceWith<T, T>(items, obj[field], _.isEqual);
            let b = _.concat(obj[field] , a);
            obj[field] = [];
            for (let i = 0; i < b.length; i++) {
                Vue.set(obj[field], i, b[i]);
            }
        }
    }
    return obj[field];
}

export function setItemOnObject<T, O extends Object>(obj: O, field: string, item?:T) {
    if (Object.prototype.hasOwnProperty.call(obj, field) && _.isObject(obj[field])) {
        if (item && !_.isEmpty(item)) {
            for (const key in item) {
                if (Object.prototype.hasOwnProperty.call(item, key) 
                    && Object.prototype.hasOwnProperty.call(obj[field], key)
                ) {
                    const element = item[key];
                    Vue.set(obj[field], key, element);
                }
            }
        }
    }
}

// export function setObject<T>(obj:T, item?:T) {
//     if (item) {
//         for (const key in item) {
//             const element = item[key];
//             Vue.set(obj, key, element);
//         }
//     }
//     return obj;
// }