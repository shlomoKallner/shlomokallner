


//import Vue from 'vue';
import {Module, VuexModule, Mutation} from 'vuex-module-decorators';
import {UserInfo, SocialInfo, ContactInfoPayload} from '../../lib/types';
import _ from 'lodash-es';
import {addItemToArray, setItemOnObject} from './utils';

export {Module, Mutation, UserInfo, SocialInfo, ContactInfoPayload, _, addItemToArray, setItemOnObject};

@Module({name: 'adminContactInfo', namespaced: true})
export default class AdminContactInfo extends VuexModule {

}