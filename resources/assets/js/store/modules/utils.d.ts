export declare function remItemFromArray<T>(array: T[], items?: T[]): T[];
export declare function addItemToArray<T, O extends Object>(obj: O, field: string, items?: T[]): any;
export declare function setItemOnObject<T, O extends Object>(obj: O, field: string, item?: T): void;
