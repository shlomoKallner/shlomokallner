
import { Module, VuexModule, Mutation } from 'vuex-module-decorators';

@Module({name: 'debug', namespaced: true})
export default class Debug extends VuexModule {
    _debug : boolean = false;

    get debug () {
        return this._debug;
    }

    @Mutation
    setAll (debug:boolean) {
        this._debug = debug;
    }
}