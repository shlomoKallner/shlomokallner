
import { Module, VuexModule } from 'vuex-module-decorators';
import Validator from '../../lib/Validator';
import _ from 'lodash-es';
import {
    EventHandler, DataType, DataRetriever, 
    ErrorObject, EventCallback, EventHandlerGenerator,
    ValidationFunc, ValidatorExtender, ValidatorExtentions
} from '../../types';

export {DataType, ValidationFunc, ValidatorExtender};

export function validatorFn (
    data: DataRetriever, callback: EventCallback, 
    rules: object, msgs: object = {}, names: object = {}, 
    extentions?: ValidatorExtentions
) : EventHandler {
    return function<T extends Event> (e : T) : boolean {
        let v = new Validator(data(), rules, msgs, names);
        if (extentions) {
            for (const key in extentions) {
                if (extentions.hasOwnProperty(key)) {
                    const element : ValidatorExtender = extentions[key];
                    //console.log(key, element, element.callback, element.msg);
                    v.extend(key, element.callback, element.msg);
                }
            }
        }
        if (!v.passes()) {
            if (e.cancelable) {
              e.preventDefault();
              e.stopPropagation();
            }
            let err : ErrorObject = v.getErrors();
            return callback(err);
        } else {
            return callback();
        }
    };
}

export function validatorGen (rules: object, msgs: object = {}, names: object = {}, extentions?: ValidatorExtentions) : EventHandlerGenerator {
    return function (data: DataRetriever, callback: EventCallback) : EventHandler {
        return validatorFn(data, callback, rules, msgs, names, extentions);
    };
}

@Module({name: 'validation', namespaced: true})
export default class Validation extends VuexModule {
    _email = {
        rules: {
            email: 'required|email'
        },
        msgs: {
            'email.required': ':attr is Required!'
        },
        names: {},
        extentions: {}
    };
    _password = {
        rules: {
            password: 'required|mypasswordregexp'
        },
        msgs: {
            'password.required': ':attr is Required!'
        },
        names: {},
        extentions: {
            'mypasswordregexp': {
                msg: ':attr sytax is invalid! (Must be of length 16 with at least 1 Large Char, 1 Small Char and 1 Number)',
                callback: function (name, value, params) {
                    _.noop(name, params);
                    let reg = /^\S*(?=\S{16,70})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$/;
                    //let self = this;
                    //console.log('d',{self, reg, name, value, params});
                    return typeof value === 'string' && value.length > 15 && value.length < 71 && reg.test(value);
                }
            }
        }
    };

    private static _text_gen_rules (name: string, min: number = 0, max: number = 0, isRequired: boolean = true) {
        let rules = {};
        let str = '';
        if (isRequired) {
            str = str + 'required|';
            //ob.msgs[name + '.required'] = ':attr is Required!';
        }
        str = str + 'string';
        //ob.msgs[name + '.string'] = ':attr Must be a String!';
        if (min > 0) {
            str = str + '|min:' + min;
            //ob.msgs[name + '.min'] = ':attr Must be a String!';
        }
        if (max > 0) {
            str = str + '|max:' + max;
            //ob.msgs[name + '.string'] = ':attr Must be a String!';
        }
        rules[name] = str;
        return rules;
    }

    _contactForm = {
        rules: {
            name: 'required|string|min:3|max:70',
            phone: 'required|string|min:3|max:70',
            message: 'required|string|min:30|max:700'
        },
        msgs: {},
        names: {},
        extentions: {}
    };

    get contactForm () {
        let rules = {
            ...Validation._text_gen_rules('name', 3, 70, true),
            ...this._email.rules,
            ...Validation._text_gen_rules('phone', 3, 70, true),
            ...Validation._text_gen_rules('message', 30, 700, true)
        };
        let msgs = {...this._email.msgs};
        let names = {...this._email.names};
        return validatorGen(rules, msgs, names);
    }


    get login () {
        let rules = {'remember': 'boolean', ...this._email.rules, ...this._password.rules};
        let msgs = {...this._email.msgs, ...this._password.msgs};
        let names = {...this._email.names, ...this._password.names};
        let extentions = {...this._password.extentions}
        return validatorGen(rules, msgs, names, extentions);
    }

    get passwordReset () {
        let rules = {...this._email.rules};
        let msgs = {...this._email.msgs};
        let names = {...this._email.names};
        return validatorGen(rules, msgs, names);
    }
}