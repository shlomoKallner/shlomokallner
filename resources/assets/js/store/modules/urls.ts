
import { Module, VuexModule, Mutation } from 'vuex-module-decorators';
import {LaravelRoutesObject} from '../../lib/types';
import * as url from 'url';

export function checkUrl (str : string) : boolean {
    let u = url.parse(str);
    let bol = false;
    let props = ['protocol', 'host'];
    for (let index = 0; index < props.length; index++) {
        const element = props[index];
        if (Object.prototype.hasOwnProperty.call(u, element)) {
            let desc = Object.getOwnPropertyDescriptor(u, element);
            bol = !!desc.value;
        }
    }
    return bol;
}

@Module({name: 'urls', namespaced: true})
export default class Urls extends VuexModule {
    _baseUrl : string = '';
    _urlMap : Map<string, string> = new Map<string, string>();

    get baseUrl () {
        return this._baseUrl;
    }

    @Mutation
    resetBaseUrl(baseUrl: string) {
        this._baseUrl = baseUrl;
    }

    get url() {
        return (name: string = '', isDir: boolean = false) => {
            if (name === 'baseUrl' || name === '') {
                return this._baseUrl;
            } else if (this._urlMap.has(name)) {
                let t = this._urlMap.get(name);
                let res;
                if (isDir) {
                    res = t.lastIndexOf('/') === t.length ? t : t + '/';
                } else {
                    res = t;
                }
                return checkUrl(res) ? res : url.resolve(this._baseUrl, res);
            } else {
                return '';
            }
        };
    }

    @Mutation
    addUrl(name:string, url:string) {
        if (name !== 'baseUrl' && name !== '' && url !== '') {
            this._urlMap.set(name, url);
        }
    }
    
    @Mutation
    setRoutes(routes: LaravelRoutesObject) {
        for (const name in routes) {
            const url = routes[name];
            if (name !== 'baseUrl' && name !== '' && url !== '') {
                this._urlMap.set(name, url);
            } else if ((name === 'baseUrl' || name === '') && url !== '') {
                this._baseUrl = url;
            }
        }
    }
}