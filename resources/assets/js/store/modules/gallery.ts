
import Vue from 'vue';
import { Module, VuexModule, Mutation } from 'vuex-module-decorators';
// import { Action } from 'vuex-module-decorators';
import {GalleryObject, GalleryPayloadObject} from '../../lib/types';
// import {GallerySingleObject} from '../../lib/types';
import _ from 'lodash-es';

@Module({name: 'gallery', namespaced: true})
export default class Gallery extends VuexModule {
    _items : GalleryObject[] = [];

    get items () : GalleryObject[] {
        return this._items;
    }

    
    _sortings : string[] = [];

    get sortings () : string[] {
        return this._sortings;
    }

    _title : string = '';

    get title () : string {
        return this._title;
    }

    @Mutation
    setAll (payload?: GalleryPayloadObject) {
        //this.reset(payload);
        //this._alerts = newAlerts;
        if (payload) {
            if (payload.items && !_.isEmpty(payload.items)) {
                this._items = [];
                for (let i = 0; i < payload.items.length; i++) {
                    Vue.set(this._items, i, payload.items[i]);
                }
            }
            if (payload.sortings && !_.isEmpty(payload.sortings)) {
                this._sortings = [];
                for (let i = 0; i < payload.sortings.length; i++) {
                    Vue.set(this._sortings, i, payload.sortings[i]);
                }
            }
            if (payload.title && !_.isEmpty(payload.title)) {
                this._title = '';
                Vue.set(this, '_title', payload.title);
            }
        }
    }

    // protected reset(payload?: GalleryPayloadObject) : void {
    //     if (payload) {
    //         if (payload.items) {
    //             this._items = [];
    //             for (let i = 0; i < payload.items.length; i++) {
    //                 Vue.set(this._items, i, payload.items[i]);
    //             }
    //         }
    //         if (payload.sortings) {
    //             this._sortings = [];
    //             for (let i = 0; i < payload.sortings.length; i++) {
    //                 Vue.set(this._sortings, i, payload.sortings[i]);
    //             }
    //         }
    //         if (payload.title) {
    //             this._title = '';
    //             Vue.set(this, '_title', payload.title);
    //         }
    //     } else {
    //         // Vue.set(this, '_items', this._items);
    //         // Vue.set(this, '_sortings', this._sortings);
    //         this.reset(
    //             {items: this._items, sortings: this._sortings, title: this._title}
    //         );
    //     }
    // }

    // @Mutation
    // remOne (item: GallerySingleObject) {
    //     this.sub(
    //         {
    //             items: item.item ? [item.item] : undefined,
    //             sortings: item.sorting ? [item.sorting] : undefined,
    //             title: undefined
    //         }
    //     );
    // }

    @Mutation
    sub (payload: GalleryPayloadObject) {
        if (payload.items && !_.isEmpty(payload.items)) {
            for (let i = 0; i < payload.items.length; i++) {
                const elem = payload.items[i];
                let idx = _.findIndex(this._items, obj => _.isEqual(elem, obj));
                if (idx >= 0) {
                    Vue.delete(this._items, idx);
                }
                // _.pull(this._items, payload.items[i]);
            }
        }
        // let pi = _.pullAll(this._items, payload.items ? payload.items : []);
        if (payload.sortings && !_.isEmpty(payload.sortings)) {
            for (let i = 0; i < payload.sortings.length; i++) {
                const elem = payload.sortings[i];
                let idx = _.findIndex(this._sortings, obj => _.isEqual(elem, obj));
                if (idx >= 0) {
                    Vue.delete(this._sortings, idx);
                }
                // _.pull(this._sortings, payload.sortings[i]);
            }
        }
        // let ps = _.pullAll(this._sortings, payload.sortings ? payload.sortings : []);
        // this.reset({items: pi, sortings: ps, title: undefined});
    }

    @Mutation
    add (payload: GalleryPayloadObject) {
        if (payload.items && !_.isEmpty(payload.items)) {
            let fi = _.differenceWith(payload.items, this._items, _.isEqual);
            if (! _.isEmpty(fi)) {
                let items = _.concat(this._items, fi);
                this._items = [];
                for (let i = 0; i < items.length; i++) {
                    Vue.set(this._items, i, items[i]);
                }
            }
        }
        // let fi = payload.items ? 
        //     _.differenceWith(payload.items, this._items, _.isEqual)
        //     : [];
        // let fs = payload.sortings ? 
        //     _.differenceWith(payload.sortings, this._sortings, _.isEqual)
        //     : [];
        if (payload.sortings && !_.isEmpty(payload.sortings)) {
            let fs = _.differenceWith(payload.sortings, this._sortings, _.isEqual);
            if (!_.isEmpty(fs)) {
                let sorts = _.concat(this._sortings, fs);
                this._sortings = [];
                for (let i = 0; i < sorts.length; i++) {
                    Vue.set(this._sortings, i, sorts[i]);
                }
            }
        }
        // this.reset(
        //     {
        //         items: _.concat(this._items, fi),
        //         sortings: _.concat(this._sortings, fs),
        //         title: undefined
        //     }
        // );
    }
}