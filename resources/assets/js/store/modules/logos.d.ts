import { VuexModule } from 'vuex-module-decorators';
import { ImageObject, LogosPayloadObject } from '../../lib/types';
export default class Logos extends VuexModule {
    _main: ImageObject;
    readonly main: ImageObject;
    setMain(newMain: ImageObject): void;
    _stick: ImageObject;
    readonly stick: ImageObject;
    setStick(newStick: ImageObject): void;
    _useStick: boolean;
    readonly useStick: boolean;
    setUseStick(newUS: boolean): void;
    setAll(logos: LogosPayloadObject): void;
    _scrollPast: number;
    readonly scrollPast: number;
    setScrollPast(scrollPast: number): void;
}
