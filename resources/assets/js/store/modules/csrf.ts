
import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators';
import {CsrfObject} from '../../lib/types';

@Module({name: 'csrf', namespaced: true})
export default class Csrf extends VuexModule {
    _csrf : string = '';
    _nut : string = '';

    get csrf () : string {
        return this._csrf;
    }

    @Mutation
    setCsrf (newCsrf: string) {
        this._csrf = newCsrf;
    }

    @Action({ commit : 'setCsrf'})
    setCsrfAsync (newCsrf: string) {
        return newCsrf;
    }

    get nut () : string {
        return this._nut;
    }

    @Mutation
    setNut (newNut: string) {
        this._nut = newNut;
    }

    @Action({ commit : 'setNut'})
    setNutAsync (newNut: string) {
        return newNut;
    }

    @Mutation
    setAll (newCsrf: CsrfObject) {
        this._csrf = newCsrf.csrf;
        this._nut = newCsrf.nut ? newCsrf.nut : null;
    }
}