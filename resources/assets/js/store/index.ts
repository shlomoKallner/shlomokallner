//import Vue from 'vue'
//import vuex, {Store} from 'vuex'
// import {Component, Watch, Prop, Emit} from 'vue-property-decorator'
//import {} from 'vuex-module-decorators'
//import {Csrf, Logos, Alerts} from './modules'
import {LogosObject, ImageObject, CsrfObject, AlertObject} from '../lib/types'

//Vue.use(vuex)
import LaravelStore from './LaravelStore'
import LaravelAdminStore from './LaravelAdminStore'

export {ImageObject, LogosObject, CsrfObject, AlertObject, LaravelStore, LaravelAdminStore};