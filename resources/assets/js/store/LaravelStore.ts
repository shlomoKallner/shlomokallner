
import {Store} from 'vuex';
import {AxiosInstance} from 'axios';
//import {VuexModule} from 'vuex-module-decorators';
import {
    Csrf, Logos, Alerts, Urls, Gallery, 
    AboutUs, Validation, Debug, ContactInfo
} from './modules';
import {
    LogosObject, CsrfObject, AlertObject, 
    GalleryPayloadObject, LaravelRoutesObject, 
    ContactInfoPayload
} from '../lib/types';
import _ from 'lodash-es';


// export function createStore(csrf: CsrfObject, logos: LogosObject, alerts: AlertObject[]): Store<any> {
//     const store = new Store<any>({
//         state: {},
//         modules: {
//             Csrf,
//             Logos,
//             Alerts
//         },
//         actions: {}
//     });
//     store.commit('csrf/setAll', csrf);
//     store.commit('logos/setAll', logos);
//     store.commit('alerts/setAlerts', alerts);
//     return store;
// }

export default class LaravelStore extends Store<any> {
    constructor(protected _axios:AxiosInstance, debug? : boolean, baseUrl?: string, csrf?: CsrfObject, logos?: LogosObject, alerts?: AlertObject[], gallery?: GalleryPayloadObject, contactInfo?: ContactInfoPayload, scrollPast: number = 50) {
        super({
            state: {},
            modules: {
                Csrf,
                Logos,
                Alerts,
                Urls, 
                Gallery,
                AboutUs, // TO DO: Add to the ctor and reset methods!
                Validation, // Validation is a 'getters and (constant) state' only module -> no need to add to ctor! 
                Debug,
                ContactInfo
            },
            actions: {},
            getters: {
                axios: function () {
                    return _axios;
                },
                lodash: function () {
                    return _;
                }
            }
        });
        if (csrf !== undefined) {
            this.commit('Csrf/setAll', csrf);
        }
        if (logos !== undefined) {
            this.commit('Logos/setAll', {logos, scrollPast});
        }
        if (alerts !== undefined) {
            this.commit('Alerts/setAlerts', alerts);
        }
        if (baseUrl !== undefined) {
            this.commit('Urls/resetBaseUrl', baseUrl);
        }
        if (gallery !== undefined) {
            this.commit('Gallery/setAll', gallery);
        }
        this.commit('Debug/setAll', debug !== undefined ? debug : false);
        if (contactInfo !== undefined) {
            this.commit('ContactInfo/resetAll', contactInfo);
        }
    }

    resetStore (routes: LaravelRoutesObject, csrf: CsrfObject, logos: LogosObject, alerts: AlertObject[], gallery: GalleryPayloadObject, contactInfo: ContactInfoPayload, scrollPast: number) {
        this.commit('Csrf/setAll', csrf);
        this.commit('Logos/setAll', {logos, scrollPast});
        this.commit('Alerts/setAlerts', alerts);
        this.commit('Urls/setRoutes', routes);
        this.commit('Gallery/setAll', gallery);
        this.commit('ContactInfo/resetAll', contactInfo);
    }
    
    async runUpdates () {
        
    }
}
