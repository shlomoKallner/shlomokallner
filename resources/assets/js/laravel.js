

export default function createCommonComponents (window, vue, store) {

  let logos = new vue(
    {
        el: '#logo',//tags.logos,
        // data: {
        //     url: this.baseUrl
        // },
        template: `<logos-component></logos-component>`,
        store
    }
  );
  
  window.addEventListener("scroll", function () {
    store.commit('Logos/setUseStick', this.scrollY > store.getters['Logos/scrollPast']);
  });
  
  let alerts = new vue(
    {
        el:'#alerts',
        template: `<alert-component :useAlerts="true"></alert-component>`,
        store
    }
  );

  let gallery = new vue(
    {
      el: '#gallery',
      template: `<gallery-component></gallery-component>`,
      store
    }
  );

  return {logos, alerts, gallery};
}