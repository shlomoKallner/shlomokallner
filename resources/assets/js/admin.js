
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import './bootstrap';

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import AdminGallery from './components/AdminGallery.vue';

import createCommonComponents from './laravel';
import LaravelAdminObject from './lib/LaravelAdminObject';
import LaravelAdminStore from './store/LaravelAdminStore';
import CKEditor from '@ckeditor/ckeditor5-vue';

window.Vue.use( CKEditor );

window.Vue.component('gallery-component', AdminGallery);

let debug = true;
let store = new LaravelAdminStore(window.axios, debug);

let {logos, alerts, gallery} = createCommonComponents(window, window.Vue, store);

window.Laravel = new LaravelAdminObject(window.Vue, store, logos, alerts, gallery);

