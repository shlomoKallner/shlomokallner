/*------------------------------------------
 Contact form
 ------------------------------------------*/

$(document).ready(function () {

    $("#contactForm").submit(function(e) {

        e.preventDefault();
        var $ = jQuery;
        var debug = false,
            usingHelpSpan = false;

        var postData 		= $(this).serializeArray(),
            formURL 		= $(this).attr("action"),
            $cfResponse 	= $('#contactFormResponse'),
            $cfsubmit 		= $("#cfsubmit"),
            cfsubmitText 	= $cfsubmit.text(),
            token           = $('head meta[name="csrf-token"]').attr('content');

        $cfResponse.empty();
        $cfsubmit.text("Sending...");

        if (debug) {
            console.log('in contact.js');
        }


        $.ajax(
            {
                url : formURL,
                type: "POST",
                data : postData,
                dataType: 'json',
                headers: {
                  'X-CSRF-TOKEN': token,
                  'X-XSRF-TOKEN': token
                },
                xhrFields: {
                  withCredentials: true
                },
                success:function(data, status, xhr)
                {
                    if (debug) {
                        console.log(status);
                    }
                    $.each(postData, function (i, field) {
                        var sel = usingHelpSpan 
                            ? '#contactForm #contactForm-' + field.name + '-error'
                            : '#contactForm #' + field.name;
                        var c = $(sel);
                        var par = c.parent('.form-group');
                        if (data.response === 'okay') {
                            par.removeClass('has-error');
                        } else {
                            if (field.name in data.errors.list) {
                                par.addClass('has-error');
                            } else {
                                par.removeClass('has-error');
                            }
                        }
                    });
                    if (data.response === 'okay') {
                        $cfResponse.html(data.html);
                    } else if (data.response === 'error') {
                        $cfResponse.empty();
                    }
                    $cfsubmit.text(cfsubmitText);
                },
                error: function (xhr, status, error)
                {
                    var str = 'Error occurred!';
                    $cfResponse.html('<p class="text-danger">' + str + ' Please try again</p>');
                    //alert("Error occurred! Please try again");
                    if (debug) {
                        console.log(str + ' : ' + error, status, xhr);
                    }
                }
            });

        return false;

    });
});


