
/*=================================
||			Owl Carousel
==================================*/
    $("#header-slider").owlCarousel({

        navigation : true, // Show next and prev buttons
        slideSpeed : 100,
        paginationSpeed : 400,
        singleItem: true,
        autoPlay: true,
        pagination: false,

        // "singleItem:true" is a shortcut for:
        // items : 1, 
        // itemsDesktop : false,
        // itemsDesktopSmall : false,
        // itemsTablet: false,
        // itemsMobile : false

    });

/*=================================
||			WOW
==================================*/
// wow = new WOW(
//     {
//       boxClass:     'wow',      // default
//       animateClass: 'animated', // default
//       offset:       0,          // default
//       mobile:       true,       // default
//       live:         true        // default
//     }
//   )
// wow.init();

/*=================================
||			Smooth Scrooling
==================================*/
	$(function() {
	    $('a[href*="#"]:not([href="#"])').click(function() {
	        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	            var target = $(this.hash);
	            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	            if (target.length) {
	                $('html,body').animate({
	                	scrollTop: (target.offset().top - 9)//top navigation height
	                }, 1000);
	                return false;
	            }
	        }
	    });
	});

	
/*====================================================================
            Navbar shrink script
======================================================================*/
$(function() {
    $(window).scroll(function() {
        if ($(document).scrollTop() > 50) {
            $('nav').addClass('shrink');
        } 
        else {
            $('nav').removeClass('shrink');
        }
    });
});


/** 
    // $(function(){
    //     $(window).on( "scroll", function() {
    //         if ($(document).scrollTop() > 50) {
    //             $("#logo").attr("src", window.Laravel.logos.stick.img)
    //         }
    //         else {
    //              $("#logo").attr("src", window.Laravel.logos.main.img)
    //         }
    //     });
    // });
*/
/*=================================================================
            Load more button
===================================================================*/

// $(function () {
//     $("#loadMenuContent").click(function(event) {
        
//         $.get("php/ajax_menu.html", function(data){
//             $('#moreMenuContent').append(data);
//         });
//         event.preventDefault();
//         $(this).hide();
//     }) ;
// });

$(function () {

    var $menuPricing = $('#menu-pricing');
    $menuPricing.mixItUp({
        selectors: {
            target: 'li'
        }
    });

});


/*=================================================
        Showing Icon in placeholder
=====================================================*/

$('.iconified').on('keyup', function() {
    var input = $(this);
    if(input.val().length === 0) {
        input.addClass('empty');
    } else {
        input.removeClass('empty');
    }
});

/*=========================================================
                Scroll  Speed
=======================================================*/

$(function() {  
    jQuery.scrollSpeed(100, 1000);
});

/*------------------------------------------
 Contact form
 ------------------------------------------*/

// $(function() {
//     $("#contactForm").submit(function(e) {
//         window.Laravel.utils.doFormAjax(
//             e, this, '#contactFormResponse', '#cfsubmit', 
//             false, true, 'json', true
//         );
//     });
// });

/*------------------------------------------
 Login forms
 ------------------------------------------*/

// $(function() {
//     $('#loginModalForm').submit(function(e) {
//         window.Laravel.utils.doFormAjax(
//             e, this, '#loginModalFormResponse', 
//             '#loginModalFormSubmit', 
//             false, true, 'json', true
//         );
//     });
//     $('#loginModalRequestForm').submit(function(e) {
//         window.Laravel.utils.doFormAjax(
//             e, this, '#loginModalRequestFormResponse', 
//             '#loginModalRequestFormSubmit', 
//             false, true, 'json', true
//         );
//     });
// });